package setup.com.readers;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class PG_Properties {

	private static final String RELATIVE_URL = "Config.properties";
	private static FileReader reader;

	public static void main(String[] args) {
		System.out.println(getProperty("UserName"));
	}

	public static String getProperty(String Key) {
		Properties prop = new Properties();

		try {
			reader = new FileReader(new File(RELATIVE_URL));
			prop.load(reader);
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return prop.getProperty(Key);

	}

}
