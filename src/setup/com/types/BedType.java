package setup.com.types;

public class BedType {
	
	public  String BedTypeName ="";
	public  String Bed_DefaultAdults ="";
	public  String Bed_Desc ="";
	public String getBedTypeName() {
		return BedTypeName;
	}
	public void setBedTypeName(String bedTypeName) {
		BedTypeName = bedTypeName;
	}
	public String getBed_DefaultAdults() {
		return Bed_DefaultAdults;
	}
	public void setBed_DefaultAdults(String bed_DefaultAdults) {
		Bed_DefaultAdults = bed_DefaultAdults;
	}
	public String getBed_Desc() {
		return Bed_Desc;
	}
	public void setBed_Desc(String bed_Desc) {
		Bed_Desc = bed_Desc;
	}

	
	
	
}
