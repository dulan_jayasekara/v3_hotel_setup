package setup.com.utilities;

import setup.com.enumtypes.ChargeByType;

public class HotelTax {
	
	private String FromT = "";
	private String ToT   = "";
	private ChargeByType SalesTaxChargeType           = null;
	private String       SalesTaxValue                = "";
	private ChargeByType OccupancyChargeType          = null;
	private String       OccupancyTaxValue            = "";
	private ChargeByType EnergyChargeType             = null;
	private String       EnergyTaxValue               = "";
	private String       MiscellaneousFees            = "";
	public String getFromT() {
		return FromT;
	}
	public void setFromT(String fromT) {
		FromT = fromT;
	}
	public String getToT() {
		return ToT;
	}
	public void setToT(String toT) {
		ToT = toT;
	}
	public ChargeByType getSalesTaxChargeType() {
		return SalesTaxChargeType;
	}
	public void setSalesTaxChargeType(ChargeByType salesTaxChargeType) {
		SalesTaxChargeType = salesTaxChargeType;
	}
	public String getSalesTaxValue() {
		return SalesTaxValue;
	}
	public void setSalesTaxValue(String salesTaxValue) {
		SalesTaxValue = salesTaxValue;
	}
	public ChargeByType getOccupancyChargeType() {
		return OccupancyChargeType;
	}
	public void setOccupancyChargeType(ChargeByType occupancyChargeType) {
		OccupancyChargeType = occupancyChargeType;
	}
	public String getOccupancyTaxValue() {
		return OccupancyTaxValue;
	}
	public void setOccupancyTaxValue(String occupancyTaxValue) {
		OccupancyTaxValue = occupancyTaxValue;
	}
	public ChargeByType getEnergyChargeType() {
		return EnergyChargeType;
	}
	public void setEnergyChargeType(ChargeByType energyChargeType) {
		EnergyChargeType = energyChargeType;
	}
	public String getEnergyTaxValue() {
		return EnergyTaxValue;
	}
	public void setEnergyTaxValue(String energyTaxValue) {
		EnergyTaxValue = energyTaxValue;
	}
	public String getMiscellaneousFees() {
		return MiscellaneousFees;
	}
	public void setMiscellaneousFees(String miscellaneousFees) {
		MiscellaneousFees = miscellaneousFees;
	}
	
	
	
}
