package setup.com.loaders;



import setup.com.utilities.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import setup.com.enumtypes.BookingChannelType;
import setup.com.enumtypes.ChargeByType;
import setup.com.enumtypes.DiscountRateApplicableType;
import setup.com.enumtypes.HotelInventoryType;
import setup.com.enumtypes.InventoryActionType;
import setup.com.enumtypes.PartnerType;
import setup.com.enumtypes.PromotionApplicablePeriodType;
import setup.com.enumtypes.PromotionBasedOnType;
import setup.com.enumtypes.PromotionSpecialRateApplicableType;
import setup.com.enumtypes.PromotionType;
import setup.runners.promotion.*;
import setup.runners.supplementary.*;




import setup.runners.supplier.Supplier;



public class DataLoader {
	
org.apache.log4j.Logger dataLogger = null;
public DataLoader()
{
	dataLogger = org.apache.log4j.Logger.getLogger(this.getClass());
}

		
public TreeMap<String,Hotel>  loadHotelDetails(Map<Integer,String> map)
{
	TreeMap<String,Hotel> hotelList = new TreeMap<String, Hotel>();

Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

while(it.hasNext())
{
	Hotel CurrentHotel = new Hotel();
	String[]   AllValues     = it.next().getValue().split(",");
	
	System.out.println(AllValues[1]);

	CurrentHotel.setHotelName(AllValues[0]);
	CurrentHotel.setSupplier(AllValues[1]);
	CurrentHotel.setHotelCurrency(AllValues[2]);
	CurrentHotel.setAddressLine1(AllValues[3]);
	CurrentHotel.setAddressLine2(AllValues[4]);
	CurrentHotel.setCountry(AllValues[5]);
	CurrentHotel.setCity(AllValues[6]);
	CurrentHotel.setHotelGroup(AllValues[7]);
	CurrentHotel.setStarCategory(AllValues[8]); 
	CurrentHotel.setFeaturedStatus(AllValues[9]);
	CurrentHotel.setPassportrequired(AllValues[10]);
	CurrentHotel.setDisplayinCC(AllValues[11]);
	CurrentHotel.setDisplayinWeb(AllValues[12]);
	CurrentHotel.setChildrenAllowed(AllValues[13]);
	CurrentHotel.setChargesFreeAgeFrom(AllValues[14]);
	CurrentHotel.setChargesFreeAgeTo(AllValues[15]);
	CurrentHotel.setChildRateApplicableFrom(AllValues[16]);
	CurrentHotel.setChildRateApplicableTo(AllValues[17]);
	CurrentHotel.setStandardCheckIn(AllValues[18]);
	CurrentHotel.setStandardCheckOut(AllValues[19]);
	CurrentHotel.setInventoryObtainedType(setup.com.enumtypes.InventoryObtainedByType.getinveType(AllValues[20]));
	CurrentHotel.setRateSetUpByType(setup.com.enumtypes.RateSetupBy.getRateSetupType(AllValues[21]));
	CurrentHotel.setRateContract(setup.com.enumtypes.RateContractByType.getRateContractType(AllValues[22]));
	CurrentHotel.setHotelContractFromDate(AllValues[23]);
	CurrentHotel.setHotelContractToDate(AllValues[24]);
	CurrentHotel.setCommHotelType(AllValues[25].trim());
	CurrentHotel.setShortDescription(AllValues[26].trim());
	CurrentHotel.setLongDescription(AllValues[27].trim());
	CurrentHotel.setLatitude(AllValues[28].trim());
	CurrentHotel.setLongitude(AllValues[29].trim());
	CurrentHotel.setFilter(AllValues[30].trim());
	CurrentHotel.setPath(AllValues[31].trim());
	CurrentHotel.setSetupNeeded(AllValues[32].trim());
	
	System.out.println(AllValues[0] +"--->"+ AllValues[32]);
	
	
	hotelList.put(AllValues[0], CurrentHotel);
}
	return hotelList;
	
}

public TreeMap<String,Hotel>  loadRoomDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	
	
    Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
   
	while(it.hasNext())
	{
		HotelRoom Room = new HotelRoom();
		String[]   AllValues     = it.next().getValue().split(",");
		Room.setRoomType(AllValues[1]);
		Room.setBedType(AllValues[2]);
		Room.setRatePlan(AllValues[3]);
		Room.setStdAdults(AllValues[4]);
		Room.setAAdults(AllValues[5]);
		Room.setChildren(AllValues[6]);
		
		if(AllValues[7].equalsIgnoreCase("yes"))
		  Room.setMultipleChildRatesApplied(true);
		else
	      Room.setMultipleChildRatesApplied(false);

        
		if(AllValues[8].equalsIgnoreCase("yes"))
	      Room.setCombinationActive(true);
		else
		  Room.setCombinationActive(false);
		
		Room.setRegion(AllValues[9]);
		Room.setTourOperator(AllValues[10]);
		Room.setTotalRooms(AllValues[11]);
		Room.setMinimumNightsStay(AllValues[12]);
		Room.setMaximumNightsStay(AllValues[13]);
		Room.setCutOff(AllValues[14]);
		Room.setNetRate(AllValues[15]);
		Room.setAdditionalAdultRate(AllValues[16]);
		Room.setChildNetRate(AllValues[17]);

        
        
        try {
        	HotelList.get(AllValues[0]).AddRoom(Room);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}

public TreeMap<String,Hotel>  loadContractDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList) throws ParseException
{
	

	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		HotelContract   Contract = new HotelContract();
		String[]   AllValues     = it.next().getValue().split(",");
     /*   Contract.setInventoryObtainedType(com.types.InventoryObtainedByType.getinveType(AllValues[1]));
        Contract.setRateSetUpByType(com.types.RateSetupBy.getRateSetupType(AllValues[2]));
        Contract.setRateContract(com.types.RateContractByType.getRateContractType(AllValues[3]));*/
        Contract.setInventoryType(HotelInventoryType.getInventoryType(AllValues[1]));
        Contract.setSearchSatistied(InventoryActionType.getInventoryActionType(AllValues[2]));
        Contract.setInventoryExhausted(InventoryActionType.getInventoryActionType(AllValues[3]));
        Contract.setCutoffApplied(InventoryActionType.getInventoryActionType(AllValues[4]));
        Contract.setMinNightRestriction(InventoryActionType.getInventoryActionType(AllValues[5]));
        Contract.setMAxNightRestriction(InventoryActionType.getInventoryActionType(AllValues[6]));
        Contract.setBlackout(InventoryActionType.getInventoryActionType(AllValues[7]));
        Contract.setNoArrival(InventoryActionType.getInventoryActionType(AllValues[8]));
      
        //Contract.setHotelCommission(AllValues[9]);
        Contract.setCommType(AllValues[12]);
        Contract.setCommvalue(AllValues[13]);
       
        Contract.setContractFrom(AllValues[10]);
        Contract.setContractTo(AllValues[11]);
        
        try {
        	HotelList.get(AllValues[0]).AddContract(Contract);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
        
	}
	
	    
		return HotelList;
	
	
}

public TreeMap<String,Hotel>  loadPolicyDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	

	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		HotelPolicy    Policy    = new HotelPolicy();
		String[]   AllValues     = it.next().getValue().split(",");

		Policy.setFrom(AllValues[1]);
		Policy.setTo(AllValues[2]);
		Policy.setArrivalLessThan(AllValues[3]);
		Policy.setCancellatrionBuffer(AllValues[4]);
		Policy.setStdChargeByType(setup.com.enumtypes.ChargeByType.getChargeType(AllValues[5].trim()));
		Policy.setStdValue(AllValues[6]);
		Policy.setNoShowChargeByType(setup.com.enumtypes.ChargeByType.getChargeType(AllValues[7].trim()));
		Policy.setNoshowValue(AllValues[8]);
 
        try {
        	HotelList.get(AllValues[0]).AddPolicy(Policy);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}

public TreeMap<String,Hotel>  loadTaxDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	

	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		HotelTax    tax   = new HotelTax();
		String[]   AllValues     = it.next().getValue().split(",");

		tax.setFromT(AllValues[1]);
		tax.setToT(AllValues[2]);
		tax.setSalesTaxChargeType(setup.com.enumtypes.ChargeByType.getChargeType(AllValues[3].trim()));
		tax.setSalesTaxValue(AllValues[4]);
		tax.setOccupancyChargeType(setup.com.enumtypes.ChargeByType.getChargeType(AllValues[5].trim()));
		tax.setOccupancyTaxValue(AllValues[6]);
		tax.setEnergyChargeType(setup.com.enumtypes.ChargeByType.getChargeType(AllValues[7].trim()));
		tax.setEnergyTaxValue(AllValues[8]);
		tax.setMiscellaneousFees(AllValues[9]);

        
        
        try {
        	HotelList.get(AllValues[0].trim()).AddTax(tax);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}

public TreeMap<String,Hotel>  loadMarkupDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	
	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		ProfitMarkup  MarkUp     = new ProfitMarkup();
		String[]   AllValues     = it.next().getValue().split(",");

		MarkUp.setBookingChannel(AllValues[1]);
		MarkUp.setPartnerType(AllValues[2]);
		MarkUp.setChargeType(ChargeByType.getChargeType(AllValues[3]));
		MarkUp.setApplyProfitMarkupTo(AllValues[4]);
		
		if(AllValues[5].equalsIgnoreCase("yes"))
		   MarkUp.setOverriteSpecific(true);
	    else
	       MarkUp.setOverriteSpecific(false);
		
	     MarkUp.setApplicablePattern(AllValues[6]);
		 MarkUp.setAdultProfitMarkup(AllValues[7]);
         MarkUp.setAdditionalAdultProfitMarkup(AllValues[8]);
		 MarkUp.setChildProfitMarkup(AllValues[9]);
         MarkUp.setFrom(AllValues[10]);
         MarkUp.setTo(AllValues[11]);

        
        try {
            HotelList.get(AllValues[0]).AddMarkup(MarkUp);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}
	
public ArrayList<Supplier>    loadSupplierDetails(Map<Integer,String> map)
{
	ArrayList<Supplier> SupplierList = new ArrayList<Supplier>();
	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		Supplier sup = new Supplier();
		String[]   AllValues     = it.next().getValue().split(",");

        sup.setSupplierCode(AllValues[0]);
		sup.setSupplierName(AllValues[1]);
		sup.setSupplierType(AllValues[2]);
		sup.setAddress(AllValues[3]);
		sup.setCountry(AllValues[4]);
		sup.setCity(AllValues[5]);
		sup.setActive(AllValues[6]);
		sup.setCurrency(AllValues[7]);
		sup.setContactName(AllValues[8]);
		sup.setEmail(AllValues[9]);
		sup.setContactMedia(AllValues[10]);
		sup.setContactType(AllValues[11]);
		
		SupplierList.add(sup);
		 
		
	}
		return SupplierList;
	
	
}

public TreeMap<String, Hotel> loadSupplimentaryDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{


Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

while(it.hasNext())
{
	
	HotelSupplementary hotelSupplementary = new HotelSupplementary();
	String[] values = it.next().getValue().split(",");
	
	hotelSupplementary.setSupplementary_Name(values[1]);
	hotelSupplementary.setSupplier_Name(values[2]);
	hotelSupplementary.setHotel_Name(values[0]);
	hotelSupplementary.setRoom_Type(values[3]);
	hotelSupplementary.setRegion(values[4]);
	hotelSupplementary.setTour_Operator(values[5]);
	hotelSupplementary.setDate_From(values[6]);
	hotelSupplementary.setDate_To(values[7]);
	hotelSupplementary.setSupplementary_Applicable(values[8]);
	hotelSupplementary.setMendatory(values[9]);
	
	hotelSupplementary.setActive(values[10]);
	hotelSupplementary.setRateBased_On(values[11]);
	hotelSupplementary.setNetRate(values[12]);
	hotelSupplementary.setChild_NetRate(values[13]);
	hotelSupplementary.setProfit_By(values[14]);
	hotelSupplementary.setProfit_Value(values[15]);
	hotelSupplementary.setChild_Profit_Value(values[16]);
	hotelSupplementary.setMin_Child_Age(values[17]);
	hotelSupplementary.setMax_Child_Age(values[18]);
	hotelSupplementary.setPercentage_Room_Rate(values[19]);
	
    try {
        HotelList.get(values[0]).addHotelSupplimentary(values[1], hotelSupplementary);
	} catch (Exception e) {
	  dataLogger.warn(values[0]+" not found in the hotel list");
	}
}
	return HotelList;
	
}

public TreeMap<String,Hotel>  loadPromoDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	
	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{

		
		HotelPromotion hotelPromotion = new HotelPromotion();
		String[] values = it.next().getValue().split(",");
		
		hotelPromotion.setSupp_Name(values[1]);
		hotelPromotion.setHotel_Name(values[0]);
		hotelPromotion.setRoom_Type(values[2]);
		hotelPromotion.setBed_Type(values[3]);
		hotelPromotion.setRate_Plan(values[4]);
		hotelPromotion.setCalculation_Logic(values[5]);
		hotelPromotion.setPromotion_BasedOn(PromotionBasedOnType.getInventoryType(values[6]));
		hotelPromotion.setNights_Book(values[7]);
		hotelPromotion.setPrior_Arrival(values[8]);
		hotelPromotion.setPromotionType(PromotionType.getInventoryType(values[9]));
		
		hotelPromotion.setFN_FreeNights(values[10]);
		hotelPromotion.setFN_MaxNumberofNights(values[11]);
		hotelPromotion.setFN_Surchage_Fee(values[12]);
		hotelPromotion.setSpecialRateApplicableType(PromotionSpecialRateApplicableType.getInventoryType(values[13]));
		hotelPromotion.setDiscountrateapplicabletype(DiscountRateApplicableType.getChargeType(values[14]));
		hotelPromotion.setPromo_Value(values[15]);
		hotelPromotion.setNote(values[16]);
		hotelPromotion.setPromoCode(values[17]);
		hotelPromotion.setIsbestrateguarantee(values[18]);
		hotelPromotion.setApplicablePeriod(PromotionApplicablePeriodType.getChargeType(values[19]));
		hotelPromotion.setBooking_Date_From(values[20]);
		hotelPromotion.setBooking_Date_To(values[21]);
		hotelPromotion.setStay_Date_From(values[22]);
		hotelPromotion.setStay_Date_To(values[23]);
		hotelPromotion.setBooking_Channel(BookingChannelType.getChargeType(values[24]));
		hotelPromotion.setPartnerType(PartnerType.getChargeType(values[25]));
		hotelPromotion.setAgentRegion(values[26]);
		hotelPromotion.setAgentName(values[27]);
		hotelPromotion.setIs_Active(values[28]);
		
		  
        try {
            HotelList.get(values[0]).addHotelPromotions(hotelPromotion);
		} catch (Exception e) {
		  dataLogger.warn(values[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}
}


