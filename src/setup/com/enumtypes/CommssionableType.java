package setup.com.enumtypes;

public enum CommssionableType {
	
	PAYDURINGBOOKING,PAYATSITE,PAYCOMMISION,NONE;
	
	public static CommssionableType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Pay During Booking"))return PAYDURINGBOOKING;
		else if(ChargeType.equalsIgnoreCase("Pay at Site"))return PAYATSITE;
		else if(ChargeType.equalsIgnoreCase("Pay Commission"))return PAYCOMMISION;
		else return NONE;
		
	}

	
}
