package setup.com.enumtypes;

public enum PartnerType {
	
	DIRECT,AFFLIATE,CORPORATE,TOUROPERATOR,TRAVELAGENT,ALL,NONE;
	
	public static PartnerType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("DIRECT"))return DIRECT;
		else if(ChargeType.equalsIgnoreCase("AFFLIATE"))return AFFLIATE;
		else if(ChargeType.equalsIgnoreCase("CORPORATE"))return CORPORATE;
		else if(ChargeType.equalsIgnoreCase("TOUR OPERATOR"))return TOUROPERATOR;
		else if(ChargeType.equalsIgnoreCase("Travel Agents"))return TRAVELAGENT;
		else if(ChargeType.equalsIgnoreCase("All"))return ALL;
		else return NONE;
		
	}

	
}
