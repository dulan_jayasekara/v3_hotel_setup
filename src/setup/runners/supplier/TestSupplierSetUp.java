package setup.runners.supplier;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.lang.String;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import setup.runners.supplier.Repository;
import setup.com.readers.ReadExcel;

public class TestSupplierSetUp {

	private WebDriver driver;
	private Map<Integer, String> Loginmap;
	private Map<Integer, String> Createmap;
	private Map<Integer, String> Contractmap;
	private static Logger logger = null;

	public TestSupplierSetUp() {

		logger = Logger.getLogger(this.getClass());
	}

	@Before
	public void setUp() throws Exception {
		File fp = new File(Repository.Firefox_Path);
		FirefoxProfile prof = new FirefoxProfile(fp);
		driver = new FirefoxDriver(prof);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testAvi() throws InterruptedException, NumberFormatException,
			ParseException {
		ArrayList<Map<Integer, String>> SheetList;
		ReadExcel ReadExl = new ReadExcel();
		SheetList = ReadExl.init(Repository.ExcelSheet_path_Hotel);
		Loginmap = SheetList.get(0);
		Createmap = SheetList.get(1);
		Contractmap = SheetList.get(2);

		if (loadContactPage(Contractmap))
			System.out.println("Contacts Loaded Successfully");
		else
			System.out.println("Error With Loading Contacts");

		Iterator<Map.Entry<Integer, String>> it = Createmap.entrySet()
				.iterator();

		while (it.hasNext()) {
			if (loadCreatePage(Createmap))
				logger.info("CreateDetails Loaded Successfully");
			else
				logger.info("Error With Loading CreateDetails");
			SupplierFlow res = new SupplierFlow();

			driver = res.loginPage(driver);
		//	driver = res.CreatePage(driver);
			

		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();

	}

	public boolean loadContactPage(Map<Integer, String> map) {
		try {
			Iterator<Map.Entry<Integer, String>> it = Contractmap.entrySet()
					.iterator();

			Map.Entry<Integer, String> pair = it.next();
			String[] ValueSet = pair.getValue().split(",");
			Repository.Contact_Name = ValueSet[0];
			Repository.Email = ValueSet[1];
			Repository.Contact_Media = ValueSet[2];
			Repository.Contact_Type = ValueSet[3];

			return true;
		} catch (Exception e) {

			return false;
		}

	}

	public boolean loadCreatePage(Map<Integer, String> map) {
		try {
			Iterator<Map.Entry<Integer, String>> it = Createmap.entrySet()
					.iterator();

			Map.Entry<Integer, String> pair = it.next();
			String[] ValueSet = pair.getValue().split(",");
			Repository.Supplier_code = ValueSet[0];
			Repository.Supplier_Name = ValueSet[1];
			Repository.Supplier_Type = ValueSet[2];
			Repository.Address_Line1 = ValueSet[3];
			Repository.Country = ValueSet[4];
			Repository.City = ValueSet[5];
			Repository.Is_Supplier_Active = ValueSet[6];
			Repository.Currency_Name = ValueSet[7];
			return true;
		} catch (Exception e) {

			return false;
		}

	}

	public boolean loadLogin(Map<Integer, String> map) {
		try {
			Iterator<Map.Entry<Integer, String>> it = Loginmap.entrySet()
					.iterator();

			Map.Entry<Integer, String> pair = it.next();
			String[] ValueSet = pair.getValue().split(",");
			Repository.BaseUrl = ValueSet[0];
			Repository.UserName = ValueSet[1];
			Repository.Password = ValueSet[2];

			return true;
		} catch (Exception e) {
			return false;
		}

	}

}
