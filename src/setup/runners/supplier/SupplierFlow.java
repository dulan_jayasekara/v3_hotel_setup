package setup.runners.supplier;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.runners.supplier.Repository;

public class SupplierFlow {
	boolean ResultsRecorded = false;
	Logger logger = null;
	public SupplierFlow ()
	{
		logger = Logger.getLogger(this.getClass());
	}

	public WebDriver loginPage(WebDriver driver) throws InterruptedException {

		driver.get(Repository.BaseUrl);
		try {
			driver.findElement(By.id("user_id")).sendKeys(Repository.UserName);
		} catch (Exception ex) {
			System.out
					.println("<p><FONT size=4 face=arial COLOR=Blue>System is Unavailable... </FONT><p>");
			System.out
					.println("<p><FONT size=4 face=arial COLOR=Blue>Trying Again... </FONT><p>");
			SupplierFlow res = new SupplierFlow();
			res.loginPage(driver);
		}
		driver.findElement(By.id("password")).sendKeys(Repository.Password);
		driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).submit();
		Thread.sleep(5000);

		try {
			driver.findElement(By.id("MainModuleMenuItem_hotels"));
			System.out
					.println("<p><FONT size=4 face=arial COLOR=Blue>Login to the system  :</FONT><FONT size=4 face=arial COLOR=Green>PASS</FONT><p>");
		} catch (Exception ex) {
			System.out
					.println("<p><FONT size=4 face=arial COLOR=Blue> Not Login to the system  :</FONT><FONT size=4 face=arial COLOR=RED>FAILED</FONT><p>");
			driver.quit();
			System.exit(1);

		}
		return driver;
	}

	public WebDriver CreatePage(WebDriver driver,Supplier sup, String Base) throws InterruptedException {
		driver.get(Base+"/admin/setup/SupplierSetupStandardPage.do?module=contract");

	   driver.findElement(By.id("screenaction_create")).click();
	   driver.findElement(By.id("suppliercode")).sendKeys(sup.getSupplierCode());
		
		driver.findElement(By.id("suppliername")).sendKeys(
				sup.getSupplierName());
		
		
/*	try {
		if (sup.getSupplierType().equals("Prepaid")) {

			driver.findElement(By.id("suppliertype_P")).click();
		} else {
			driver.findElement(By.id("suppliertype_C")).click();
		}
	} catch (Exception e) {
		// TODO: handle exception
	}*/
		
		
		driver.findElement(By.id("addressline1")).sendKeys(sup.getAddress().trim());
		
		//Select the Country
		driver.findElement(By.id("country")).sendKeys(sup.getCountry());
		driver.findElement(By.id("country_lkup")).click();;
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		

		// select the city
		driver.findElement(By.id("city")).sendKeys(sup.getCity());
		driver.findElement(By.id("city_lkup")).click();
		driver.switchTo().frame(driver.findElement(By.id("lookup")));
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		//WebDriverWait Mywait6 = new WebDriverWait(driver, 60);
		driver.switchTo().defaultContent();
		
		if (sup.isActive()) {

			driver.findElement(By.id("supplieractive_Y")).click();
		} else {
			driver.findElement(By.id("supplieractive_N")).click();
		}
		
		// select the Currency
		
		driver.findElement(By.id("currency")).sendKeys(sup.getCurrency());
		driver.findElement(By.id("currency_lkup")).click();
		driver.switchTo().frame(driver.findElement(By.id("lookup")));
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		
		//Add new Contact
		
		driver.switchTo().frame("suppliercontactdetails");
		driver.findElement(By.id("contactDetailsentry")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("dialogwindow");
		driver.findElement(By.id("contactname")).sendKeys(sup.getContactName());
		driver.findElement(By.id("email")).sendKeys(sup.getEmail());
		
		
		if (sup.getContactMedia().equals("Email")) {

			driver.findElement(By.id("confirmationtype_E")).click();
			
		} else if(sup.getContactMedia().equals("Fax")){
			
			driver.findElement(By.id("confirmationtype_F")).click();
		}
		else if(sup.getContactMedia().equals("Phone")){
			
			driver.findElement(By.id("confirmationtype_P")).click();
		}
		else{
			driver.findElement(By.id("confirmationtype_N")).click();
		}
		

		new Select(driver.findElement(By.name("contactType")))
		.selectByVisibleText(sup.getContactType().trim());
		
		driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input")).click();
		driver.switchTo().defaultContent();
		
		
		
		driver.findElement(By.name("submit")).click();
	//	driver.findElement(By.name("submit")).click();
	    WebDriverWait wait = new WebDriverWait(driver, 60);
	
		 try {
			    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
			   
			    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
			    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					System.out.println(TextMessage);
					logger.info(sup.getSupplierName() +" Supplier SetUp Status - DONE  Message Out :----->"+TextMessage);
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			    }
			    else
			    {
			    	
			    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					System.out.println(TextMessage);
					logger.fatal(sup.getSupplierName() +" Supplier SetUp Status - Failed  Message Out :----->"+TextMessage);
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
			    }
			    
			} catch (Exception e) {
				logger.fatal(sup.getSupplierName() +" Supplier SetUp Status - - Failed  Message Out :----->"+e.toString());
				System.out.println(e.toString());
				
			}
		
		//System.out.println("Completed");

		return driver;
	}

	public boolean isElementPresent(By by, WebElement element, WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			element.findElement(by);
			return true;
		} catch (Exception ex) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}

}
