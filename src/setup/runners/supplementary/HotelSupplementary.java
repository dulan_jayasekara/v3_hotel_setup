package setup.runners.supplementary;

public class HotelSupplementary {
	
	public String Supplementary_Name;
	public String Supplier_Name;
	public String Hotel_Name;
	public String Room_Type;
	public String Region;
	public String Tour_Operator;
	public String Date_From;
	public String Date_To;
	public String Supplementary_Applicable;
	public String Mendatory;
	
	public String Active;
	public String RateBased_On;
	public String NetRate;
	public String Child_NetRate;
	public String Profit_By;
	public String Profit_Value;
	public String Child_Profit_Value;
	public String Min_Child_Age;
	public String Max_Child_Age;
	
	public String getMin_Child_Age() {
		return Min_Child_Age;
	}
	public void setMin_Child_Age(String min_Child_Age) {
		Min_Child_Age = min_Child_Age;
	}
	public String getMax_Child_Age() {
		return Max_Child_Age;
	}
	public void setMax_Child_Age(String max_Child_Age) {
		Max_Child_Age = max_Child_Age;
	}
	public String Percentage_Room_Rate;
	
	
	
	public String getPercentage_Room_Rate() {
		return Percentage_Room_Rate;
	}
	public void setPercentage_Room_Rate(String percentage_Room_Rate) {
		Percentage_Room_Rate = percentage_Room_Rate;
	}
	public String getSupplementary_Name() {
		return Supplementary_Name;
	}
	public void setSupplementary_Name(String supplementary_Name) {
		Supplementary_Name = supplementary_Name;
	}
	public String getSupplier_Name() {
		return Supplier_Name;
	}
	public void setSupplier_Name(String supplier_Name) {
		Supplier_Name = supplier_Name;
	}
	public String getHotel_Name() {
		return Hotel_Name;
	}
	public void setHotel_Name(String hotel_Name) {
		Hotel_Name = hotel_Name;
	}
	public String getRoom_Type() {
		return Room_Type;
	}
	public void setRoom_Type(String room_Type) {
		Room_Type = room_Type;
	}
	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		Region = region;
	}
	public String getTour_Operator() {
		return Tour_Operator;
	}
	public void setTour_Operator(String tour_Operator) {
		Tour_Operator = tour_Operator;
	}
	public String getDate_From() {
		return Date_From;
	}
	public void setDate_From(String date_From) {
		Date_From = date_From;
	}
	public String getDate_To() {
		return Date_To;
	}
	public void setDate_To(String date_To) {
		Date_To = date_To;
	}
	public String getSupplementary_Applicable() {
		return Supplementary_Applicable;
	}
	public void setSupplementary_Applicable(String supplementary_Applicable) {
		Supplementary_Applicable = supplementary_Applicable;
	}
	public String getMendatory() {
		return Mendatory;
	}
	public void setMendatory(String mendatory) {
		Mendatory = mendatory;
	}
	public String getActive() {
		return Active;
	}
	public void setActive(String active) {
		Active = active;
	}
	public String getRateBased_On() {
		return RateBased_On;
	}
	public void setRateBased_On(String rateBased_On) {
		RateBased_On = rateBased_On;
	}
	public String getNetRate() {
		return NetRate;
	}
	public void setNetRate(String netRate) {
		NetRate = netRate;
	}
	public String getChild_NetRate() {
		return Child_NetRate;
	}
	public void setChild_NetRate(String child_NetRate) {
		Child_NetRate = child_NetRate;
	}
	public String getProfit_By() {
		return Profit_By;
	}
	public void setProfit_By(String profit_By) {
		Profit_By = profit_By;
	}
	public String getProfit_Value() {
		return Profit_Value;
	}
	public void setProfit_Value(String profit_Value) {
		Profit_Value = profit_Value;
	}
	public String getChild_Profit_Value() {
		return Child_Profit_Value;
	}
	public void setChild_Profit_Value(String child_Profit_Value) {
		Child_Profit_Value = child_Profit_Value;
	}
	
	


}
