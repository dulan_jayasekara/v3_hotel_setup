package setup.runners.mainsetup;

import setup.runners.basic.*;
import setup.runners.supplier.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.FileReader;
import java.lang.String;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import setup.com.enumtypes.RateContractByType;
import setup.com.loaders.*;
import setup.com.readers.*;
import setup.com.utilities.*;


public class TestHotelSetUp {

	private WebDriver driver;
	private FirefoxProfile prof = null;

	private Map<String, String> Propertymap;
	private Map<Integer, String> Createmap;
	private Map<Integer, String> Contractmap;
	private Map<Integer, String> Policymap;
	private Map<Integer, String> Taxmap;
	private Map<Integer, String> Occupancymap;
	private Map<Integer, String> PMmap;
	private Map<Integer, String> Suppliermap;
	private Map<Integer, String> Supplimentarymap;
	private Map<Integer, String> Promotionmap;
	private ArrayList<Supplier> SupplierList;
	private static Logger logger = null;
	private TreeMap<String, Hotel> HotelList;
	private BasicSetUpDriver BasicDriver = null;
//	private StringBuffer     ReprotPrinter;

	@Before
	public void setUp() throws Exception {
   //     ReprotPrinter = new StringBuffer();    
		logger        = Logger.getLogger(this.getClass());
		try {
			Propertymap = loadProperties("../Hotel_Setup_Details/Config.properties");
			// Propertymap = loadProperties("Config.properties");
			DOMConfigurator.configure("ConfigLog/log4j.xml");
		} catch (Exception e) {
			logger.fatal("Error When Loading Properties");
			logger.fatal(e.toString());
		}

		if (Propertymap.get("Driver.Type").equals("Firefox")) {
			if (Propertymap.get("Profile.Use").equals("true"))
				prof = new FirefoxProfile(new File(
						Propertymap.get("Profile.Path")));
			else
				prof = new FirefoxProfile();
			driver = new FirefoxDriver(prof);
		} else {
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					Propertymap.get("Phantom.Bin"));
			driver = new PhantomJSDriver(cap);
		}

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		if (Propertymap.get("Window.maximized").trim().equalsIgnoreCase("yes")) {
		driver.manage().window().maximize();	
		}
		loadDetails();

	}

	public void loadDetails() throws Exception {

		ArrayList<Map<Integer, String>> SheetList = null;
		ArrayList<Map<Integer, String>> SupList = null;
		;
		ArrayList<Map<Integer, String>> BasicList = null;
		;
		ReadExcel ReadExl = new ReadExcel();
		DataLoader Loader = new DataLoader();

	//	if (Propertymap.get("Hotel.Setup").trim().equalsIgnoreCase("enable")) {
			SheetList = ReadExl.init(Propertymap.get("Create.Excel.Path"));
			Createmap = SheetList.get(0);
			Contractmap = SheetList.get(1);
			Policymap = SheetList.get(2);
			Taxmap = SheetList.get(5);
			Occupancymap = SheetList.get(3);
			PMmap = SheetList.get(4);
			Supplimentarymap = SheetList.get(6);
			Promotionmap = SheetList.get(7);

			// Loading Hotel Details
			try {
				HotelList = Loader.loadHotelDetails(Createmap);
				logger.info("Hotel Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Contract Details

			try {
				HotelList = Loader.loadContractDetails(Contractmap, HotelList);
				logger.info("Hotel Contracts Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Contracts not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Policy Details
			try {
				HotelList = Loader.loadPolicyDetails(Policymap, HotelList);
				logger.info("Hotel policies Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Policies not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Tax Details

			try {
				HotelList = Loader.loadTaxDetails(Taxmap, HotelList);
				logger.info("Tax Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Tax Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Room Details

			try {
				HotelList = Loader.loadRoomDetails(Occupancymap, HotelList);
				logger.info("Hotel Room Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Room Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}
			// Loading Profit Markup Details

			try {
				HotelList = Loader.loadMarkupDetails(PMmap, HotelList);
				logger.info("Hotel Markup Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Markup Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			if (Propertymap.get("Supplimentary.Enabled").equalsIgnoreCase(
					"enable")) {

				try {
					HotelList = Loader.loadSupplimentaryDetails(
							Supplimentarymap, HotelList);
					logger.info("Hotel Supplimentary Details Loaded SuccessFully");
					logger.debug(HotelList);
				} catch (Exception e) {
					logger.fatal("Hotel Supplimentary Details Loading Failed");
					logger.fatal(e.toString());
				}
			}

			if (Propertymap.get("Promotion.Enabled").equalsIgnoreCase("enable")) {
				try {
					HotelList = Loader
							.loadPromoDetails(Promotionmap, HotelList);
					logger.info("Hotel Promotion Details Loaded SuccessFully");
					logger.debug(HotelList);
				} catch (Exception e) {
					logger.fatal("Hotel Promotion Details not Loaded SuccessFully");
					logger.fatal(e.toString());
				}
			}

		//}

		if (Propertymap.get("Supplier.Setup").equalsIgnoreCase("enable")) {
			SupList = ReadExl.init(Propertymap.get("Supplier.Excel.Path"));
			Suppliermap = SupList.get(0);
			SupplierList = Loader.loadSupplierDetails(Suppliermap);
			// SupplierContractmap = SupList.get(2);
		}

		if (Propertymap.get("Basic.Setup").trim().equalsIgnoreCase("enable")) {
			BasicList = ReadExl.init(Propertymap.get("Basic.Excel.Path"));
			BasicDriver = new BasicSetUpDriver();
			BasicDriver.setUp(Propertymap, BasicList);
		}
	}

	@Test
	public void testAvi() throws InterruptedException, NumberFormatException,
			ParseException, Exception {

		CreateSetupFlow Flow = new CreateSetupFlow(Propertymap);
		Flow.loginPage(driver);

		try {
			if (Propertymap.get("Supplier.Setup").trim()
					.equalsIgnoreCase("enable")) {
				SupplierFlow supFlow = new SupplierFlow();
				Iterator<Supplier> it = SupplierList.iterator();

				while (it.hasNext()) {
					Supplier supplier = (Supplier) it.next();
					supFlow.CreatePage(driver, supplier,
							Propertymap.get("Hotel.BaseUrl"));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			if (Propertymap.get("Basic.Setup").trim()
					.equalsIgnoreCase("enable")) {
				BasicDriver.hotelSetup(driver);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		Iterator<Map.Entry<String, Hotel>> it = (Iterator<Entry<String, Hotel>>) HotelList
				.entrySet().iterator();
		for (int i = 0; i < Integer.parseInt(Propertymap
				.get("Hotel.Starting.index")); i++) {
			it.remove();

		}
		
		//Creating Screenshot Directory	
	    String                    screenpath              = "Screenshots/HotelSetup";
		File                      screenshotdir           = new File(screenpath);
				
		
		if (!screenshotdir.exists()) {
			logger.info("Screenshot path not available - Creating directories..");

			try {
				screenshotdir.mkdirs();
				logger.info("Directory created succcessfully!!");
			} catch (Exception e) {
				logger.fatal("Directory creation failed " + e.toString());
			}
		}
		
		while (it.hasNext()) {

			Hotel currentHotel = it.next().getValue();
			Flow.setHotel(currentHotel);
			logger.info("Current Hotel Name ---->"+currentHotel.getHotelName());
			logger.info("Current set up needed ---->"+currentHotel.isSetupNeeded());
			
			//if (currentHotel.isSetupNeeded()&& (Propertymap.get("Hotel.Setup").equalsIgnoreCase("enable"))) {
			if (currentHotel.isSetupNeeded()) {
				
				String   ScenarioUrl    = screenpath+"/"+currentHotel.getHotelName().trim();
				File     ScenarioPath   = new File(ScenarioUrl);
				Flow.setScenarioUrl(ScenarioUrl);
				 if(!ScenarioPath.exists()){
					logger.info("Screenshot path not available - Creating directories..");
					
					try {
					 ScenarioPath.mkdirs();
					 logger.info("Directory created succcessfully!!");
					 } catch (Exception e) {
					 logger.fatal("Directory creation failed "+ e.toString());
					}
				}
             
				try {
                     
					if (Propertymap.get("Hotel.Setup").trim().equalsIgnoreCase("enable")) {
						logger.info("Hotel SetUp Status-->Enabled");
						{
							driver = Flow.CreatePage(driver);
							driver = Flow.ContractPage(driver);
							driver = Flow.policyPage(driver);

							driver = Flow.OccupancyPage(driver);
							driver = Flow.addInventory(driver);
							driver = Flow.addRates(driver);

							
							
					
							
							
							try {
								driver = Flow.AmenitiesPage(driver);

							} catch (Exception e) {
								logger.fatal("Error when Adding Amenities"
										+ e.toString());
							}
							
							try {
								driver = Flow.FiltersPage(driver);

							} catch (Exception e) {
								logger.fatal("Error when Adding Filters"
										+ e.toString());
							}
							
							try {
								driver = Flow.ContentPage(driver);

							} catch (Exception e) {
								logger.fatal("Error when Adding Content"
										+ e.toString());
							}
							
							try {
								driver = Flow.ImagesPage(driver);

							} catch (Exception e) {
								logger.fatal("Error when Adding Images"
										+ e.toString());
							}

							if (currentHotel.getRateContract() != RateContractByType.COMMISSIONABLE) {
								logger.info("Hotel is non commissionable Adding Profit Mark up");
								driver = Flow.ProfiMarkUpPage(driver);
							} else {
								logger.info("Hotel is commissionable not Adding Profit Mark up");
							}
						}
					} else {
						logger.info("Hotel Set up flow disabled Skipping setup");
					}
					
					
					if (Propertymap.get("Supplimentary.Enabled").equalsIgnoreCase("enable")) 
					driver = Flow.createSupplementarySetup(driver);
				
                    if (Propertymap.get("Promotion.Enabled").equalsIgnoreCase("enable")) 
					driver = Flow.createPromotionSetup(driver);
				

				} catch (Exception e) {
					logger.fatal("Error with Hotel Setup of the hotel"
							+ currentHotel.getHotelName());
					logger.fatal(e.toString());
					Flow.takeScreenshot(ScenarioUrl+"/SetupFaliure.jpg", driver);
					{
						Flow.DeleteHotel(driver, currentHotel.getHotelName());
					}
				}
               
			}else{
				logger.info("Current Hotel Set Up not needed Skipping Set Up Flow ---->"+currentHotel.isSetupNeeded());
			}
			

		}
		
		
		

	}

	@After
	public void tearDown() throws Exception {
		driver.quit();

	}

	public Map<String, String> loadProperties(String filepath) {

		Map<String, String> PropertyMap = new HashMap<String, String>();
		try {
			Properties prop = new Properties();
			FileReader fs = new FileReader(new File(filepath));
			prop.load(fs);

			for (String key : prop.stringPropertyNames()) {
				PropertyMap.put(key, prop.getProperty(key));
			}

		} catch (Exception e) {
			System.out.println("Error With Adding Properties to the Map");
			
		}
		return PropertyMap;
	}

}
