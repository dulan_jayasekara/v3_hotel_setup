package setup.runners.mainsetup;

public class Repository {

	public static String ExcelSheet_path_Hotel = "/home/madushani/workspace/V3HotelSetUp/Excel/SetUp_Details.xls";
	public static String Firefox_Path = "home/madushani/.mozilla/firefox/jb19uk12.default";

	public static final String SEARCH_RANDOM = "Random";
	public static String ProductCode = "";
	public static String HotelName = "";
	public static String SupplierName = "";
	public static String AddressLine1= "";
	public static String AddressLine2 = "";
	public static String From = "";
	public static String To = "";
	public static String Country = "";
	public static String City= "";
	public static String InventoryType= "";
	public static String BaseUrl = "http://qav3.rezgateway.com/rezbase_v3/admin/common/LoginPage.do";
	public static String User_Name = "";
	public static String Password = "";
	public static String RatesSetup = "";
	public static String RateContract = "";
	public static String Scenario_Deatils= "";
	public static String Scenario_Number= "";
	
	public static String Region = "";
	public static String ChildrenAllowed = "";
	public static String ChargesFreeAgeGroupFrom = "";
	public static String ChargesFreeAgeGroupTo = "";
	public static String ChildRateApplicableAgeGroupFrom = "";
	public static String ChildRateApplicableAgeGroupTo = "";
	public static String CheckinTime = "";
	public static String CheckoutTime = "";
	public static String HotelWeekdayClassificatioFrom = "";
	public static String HotelWeekdayClassificatioTo = "";
	public static String HotelWeekendClassificationFrom  = "";
	public static String HotelWeekendClassificationTo  = "";
	public static String ArrivallessthanDate = "";
	public static String Cancellationbuffer = "";
	public static String StandardCancellation = "";
	public static String SCNights = "";
	public static String NoShowCancellation= "";
	public static String NSNights="";
	public static String SalesTax="";
	public static String OccupancyTax = "";
	public static String EnergyTax= "";
	
	public static String RoomType= "";
	public static String BedType= "";
	public static String RatePlan= "";
	public static String Standard_Adults= "";
	public static String Additional_Adults= "";
	public static String Children= "";
	public static String MultipleChildRates= "";
	public static String CombinationActive 	= "";
	public static String Tour_Operator 	= "";
	
	public static String Booking_Channel= "";
	public static String Partner= "";
	public static String PM_Type= "";
	public static String ApplyPMTo= "";
	public static String Overwrite_Specific_Markup= "";
	public static String FixProfitMarkup= "";
	public static String ProfitMarkUp= "";
	public static String AdditionalAdulPM= "";
	public static String ChildPM= "";
	
	public static String NetRate= "";
	public static String AAdultsRate= "";
	public static String TaxFee= "";
	public static String Amount= "";
	
	
	
	
}
