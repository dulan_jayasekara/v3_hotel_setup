package setup.runners.mainsetup;


//import hotelpromotion.PG_Properties;
import java.awt.Frame;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.servlet.jsp.tagext.TryCatchFinally;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.jetty.html.Break;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.GetScreenOrientation;
import org.openqa.selenium.server.SpecialCommand;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.enumtypes.ChargeByType;
import setup.com.enumtypes.CommssionableType;
import setup.com.enumtypes.DiscountRateApplicableType;
import setup.com.enumtypes.HotelInventoryType;
import setup.com.enumtypes.InventoryActionType;
import setup.com.enumtypes.InventoryObtainedByType;
import setup.com.enumtypes.PartnerType;
import setup.com.enumtypes.PromotionApplicablePeriodType;
import setup.com.enumtypes.PromotionBasedOnType;
import setup.com.enumtypes.PromotionSpecialRateApplicableType;
import setup.com.enumtypes.PromotionType;
import setup.com.enumtypes.RateContractByType;
import setup.com.enumtypes.RateSetupBy;

import setup.com.types.*;
import setup.runners.mainsetup.*;
import setup.com.utilities.*;
import setup.runners.promotion.*;
import setup.runners.supplementary.*;


@SuppressWarnings("unused")
public class CreateSetupFlow {
	
	private boolean ResultsRecorded = false;
    private Map<String, String> PropertySet;
	private Hotel CurrentHotel;
	private Logger logger;
	private String ScenarioUrl = null;

	public CreateSetupFlow(Map<String, String> Properties) {
		PropertySet = Properties;
		logger = Logger.getLogger(this.getClass());
	}

	public void setHotel(Hotel hotel) {
		CurrentHotel = hotel;
	}
	

	public String getScenarioUrl() {
		return ScenarioUrl;
	}

	public void setScenarioUrl(String scenarioUrl) {
		ScenarioUrl = scenarioUrl;
	}

	public WebDriver loginPage(WebDriver driver) throws InterruptedException {
		logger.info("Getting URL-->" + PropertySet.get("Hotel.BaseUrl")
				+ "/admin/common/LoginPage.do");
		logger.info("Loggin With User :" + PropertySet.get("Hotel.UserName")
				+ " pass:" + PropertySet.get("Hotel.Password"));
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/admin/common/LoginPage.do");
		try {
			driver.findElement(By.id("user_id")).clear();
			driver.findElement(By.id("user_id")).sendKeys(
					PropertySet.get("Hotel.UserName"));
			logger.info("Navigating to the login page");
		} catch (Exception ex) {
			logger.fatal("System is Unavailable");
			takeScreenshot(ScenarioUrl+"/SystemNotAvailable.jpg", driver);

		}
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(
				PropertySet.get("Hotel.Password"));
		driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).submit();

		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions
					.presenceOfElementLocated(By
							.id("MainModuleMenuItem_hotels")));
			driver.findElement(By.id("MainModuleMenuItem_hotels"));
			logger.info("Logged in to the system....");

		} catch (Exception ex) {
			logger.fatal("Login failed");
			takeScreenshot(ScenarioUrl+"/LoginFailed.jpg", driver);
			driver.quit();
			System.exit(1);

		}
		return driver;
	}

	public WebDriver CreatePage(WebDriver driver) throws InterruptedException {
		
		
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupStandardPage.do?module=contract");
		acceptAlert(driver);

		try {
			driver.findElement(By.id("screenaction_create")).click();
			logger.info("Navigate to the Hotel creation page");
			takeScreenshot(ScenarioUrl+"/HotelCreatePageLoadSuccess.jpg", driver);
		} catch (Exception e) {
			takeScreenshot(ScenarioUrl+"/HotelCreatePageFailed.jpg", driver);
			logger.info("Navigation to the Hotel creation page Failed");
		}

		// Basic Details
		try {
			driver.findElement(By.id("hotelname")).sendKeys(
					CurrentHotel.getHotelName());
			driver.findElement(By.id("supplierName")).sendKeys(
					CurrentHotel.getSupplier().trim());
			driver.findElement(By.id("supplierName_lkup")).click();
			driver.switchTo().frame(driver.findElement(By.id("lookup")));
			try {
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			} catch (Exception e) {
				logger.fatal("Supplier not Exsist");
				takeScreenshot(ScenarioUrl+"/HotelCreatePageSupplierLookUp.jpg", driver);
			}
			
		/*	Thread.sleep(5000);
			//============================================================================
			driver.findElement(By.id("currencyCode")).sendKeys("USD");
			driver.findElement(By.id("currencyCode_lkup")).click();
			driver.switchTo().frame(driver.findElement(By.id("lookup")));
			try {
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			} catch (Exception e) {
				logger.fatal("Currency not Exsist");
				takeScreenshot(ScenarioUrl+"/HotelCreatePageSupplierLookUp.jpg", driver);
			}
			//=============================================================================
*/			driver.switchTo().defaultContent();
			driver.findElement(By.id("addressline1")).sendKeys(
					CurrentHotel.getAddressLine1());
			driver.findElement(By.id("addressline2")).sendKeys(
					CurrentHotel.getAddressLine2());

			// Select the Country

			driver.findElement(By.id("country")).sendKeys(
					CurrentHotel.getCountry().trim());
			driver.findElement(By.id("country_lkup")).click();
			driver.switchTo().frame(driver.findElement(By.id("lookup")));
			driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			driver.switchTo().defaultContent();

			// select the city

			driver.findElement(By.id("city")).sendKeys(
					CurrentHotel.getCity().trim());
			driver.findElement(By.id("city_lkup")).click();
			driver.switchTo().frame(driver.findElement(By.id("lookup")));
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();

			// select the HotelGroup

			driver.findElement(By.id("hotelGroup")).sendKeys(
					CurrentHotel.getHotelGroup().trim());
			driver.findElement(By.id("hotelGroup_lkup")).click();
			driver.switchTo().frame(driver.findElement(By.id("lookup")));
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();

			// select the starCategory

			driver.findElement(By.id("starCategoryName")).sendKeys(
					CurrentHotel.getStarCategory().trim());
			driver.findElement(By.id("starCategoryName_lkup")).click();
			driver.switchTo().frame(driver.findElement(By.id("lookup")));
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();

			if (CurrentHotel.isFeaturedStatus())
				driver.findElement(By.id("featuredHotel_Y")).click();
			else
				driver.findElement(By.id("featuredHotel_N")).click();

			if (CurrentHotel.isPassportrequired())
				driver.findElement(By.id("passportRequired_Y")).click();
			else
				driver.findElement(By.id("passportRequired_N")).click();

			if (CurrentHotel.isDisplayinCC())
				driver.findElement(By.id("displayincallcenter_Y")).click();
			else
				driver.findElement(By.id("displayincallcenter_N")).click();

			if (CurrentHotel.isDisplayinWeb())
				driver.findElement(By.id("displayinwebsite_Y")).click();
			else
				driver.findElement(By.id("displayinwebsite_N")).click();

			logger.info("Clicking on Save Button");
			driver.findElement(By.name("submit")).click();

			WebDriverWait wait = new WebDriverWait(driver, 30);
			// driver.findElement(By.xpath(".//*[@id='saveButId']")).click();

			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id("dialogMsgText")));
				// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(By.id("dialogMsgText"))
							.getText();
					// System.out.println(TextMessage);
					logger.info(CurrentHotel.getHotelName()
							+ " Basic Details Saving Status - DONE  Message Out :----->"
							+ TextMessage);
					driver.findElement(
							By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
							.click();
					takeScreenshot(ScenarioUrl+"/HotelCreatePageDetailsSaved.jpg", driver);
				} else {

					String TextMessage = driver.findElement(
							By.id("MainMsgBox_msgDisplayArea")).getText();
					// System.out.println(TextMessage);
					logger.fatal(CurrentHotel.getHotelName()
							+ " Basic Details Saving Status - Failed  Message Out :----->"
							+ TextMessage);
					driver.findElement(
							By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
							.click();
					takeScreenshot(ScenarioUrl+"/HotelCreatePageSavingFailed.jpg", driver);
				}

			} catch (Exception e) {
				logger.fatal(CurrentHotel.getHotelName()
						+ " Basic Details Saving Status - Failed  Message Out :----->"
						+ e.toString());
				takeScreenshot(ScenarioUrl+"/HotelCreatePageSavingFailed.jpg", driver);
				// System.out.println(e.toString());

			}
		} catch (Exception e) {
			logger.fatal(CurrentHotel.getHotelName()
					+ " Creation Page - Failed  Message Out :----->"
					+ e.toString());
			takeScreenshot(ScenarioUrl+"/HotelCreatePageFailed.jpg", driver);
			e.printStackTrace();
		}

		return driver;
	}

	public WebDriver ContractPage(WebDriver driver) throws InterruptedException {

		logger.info("Starting Setting Up Contract");
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupContractPage.do");

		// ***************************************************************************************

		try {
			driver.findElement(By.id("hotelname")).clear();
			logger.info("Navigate to Contract Page");
			driver.findElement(By.id("hotelname")).sendKeys(
					CurrentHotel.getHotelName());
			driver.findElement(By.id("hotelname_lkup")).click();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			logger.fatal("Contract Page Navigation Failed-->" + e.toString());
			takeScreenshot(ScenarioUrl+"/HotelContractPageNavigationFailed.jpg", driver);
		}

		// ***************************************************************************************

		// Inventory By
		if (CurrentHotel.getInventoryObtainedType() == InventoryObtainedByType.BYHOTEL)
			driver.findElement(By.id("inventoryby_H")).click();
		else if (CurrentHotel.getInventoryObtainedType() == InventoryObtainedByType.BYROOM)
			driver.findElement(By.id("inventoryby_R")).click();
		else if (CurrentHotel.getInventoryObtainedType() == InventoryObtainedByType.BYROOMBED)
			driver.findElement(By.id("inventoryby_B")).click();

		// RateSetup By
		if (CurrentHotel.getRateSetUpByType() == RateSetupBy.PERROOM) {
			driver.findElement(By.id("inventryBy_R")).click();

			try {
				driver.switchTo().defaultContent();
				driver.findElement(By
						.xpath("/html/body/div[6]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td/a/img"));
				((JavascriptExecutor) driver)
						.executeScript("closeDialogMsg(dialogMsgBox);");
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else if (CurrentHotel.getRateSetUpByType() == RateSetupBy.PERPERSON) {
			driver.findElement(By.id("inventryBy_P")).click();

			try {
				driver.findElement(By
						.xpath(".//*[@id='dialogMsgActionButtons']/a[1]/img"));
				((JavascriptExecutor) driver)
						.executeScript("closeDialogMsg(dialogMsgBox);");
			} catch (Exception e) {
				logger.warn("Rate Contract Change Alert not available");
			}
		}
		// Rate Contract
		if (CurrentHotel.getRateContract() == RateContractByType.NETRATE)
			driver.findElement(By.id("contractType_N")).click();
		else if (CurrentHotel.getRateContract() == RateContractByType.COMMISSIONABLE) {

			try {

				driver.findElement(By.id("contractType_C")).click();
				logger.warn("Clicked on contract type");
				Thread.sleep(3000);
				if (CurrentHotel.getCommHotelType() == CommssionableType.PAYATSITE)
					driver.findElement(By.id("commissionableContractType_S"))
							.click();
				else if (CurrentHotel.getCommHotelType() == CommssionableType.PAYDURINGBOOKING) {
					driver.findElement(By.id("commissionableContractType_B"))
							.click();

				} else if (CurrentHotel.getCommHotelType() == CommssionableType.PAYCOMMISION)
					driver.findElement(By.id("commissionableContractType_C"))
							.click();

			} catch (Exception e) {
				logger.fatal("Error on clicking commissionable type:"
						+ e.toString());
			}
		}
		ArrayList<HotelContract> ContractList = CurrentHotel
				.getHotelContracts();
		logger.info(CurrentHotel.getHotelName() + "Number Of Contracts->"
				+ ContractList.size());

		for (HotelContract hotelContract : ContractList) {
			logger.info("Adding Contract------>" + hotelContract);
			driver.switchTo().frame(
					driver.findElement(By.id("hotelcontractdetails")));
			driver.findElement(
					By.xpath("html/body/table/tbody/tr[1]/td/table/tbody/tr/td"))
					.click();

			driver.switchTo().defaultContent();
			driver.switchTo().frame("dialogwindow");

			driver.findElement(By.id("regionArray_ALL")).click();
			
			
		
			new Select(driver.findElement(By.id("contractfrom_Year_ID")))
			.selectByVisibleText(hotelContract.getContractFrom().split(
					"-")[2]);
			new Select(driver.findElement(By.id("contractfrom_Month_ID")))
			.selectByVisibleText(hotelContract.getContractFrom().split(
					"-")[1]);
            new Select(driver.findElement(By.id("contractfrom_Day_ID")))
					.selectByVisibleText(hotelContract.getContractFrom().split(
							"-")[0]);
		
			
           
            
            new Select(driver.findElement(By.id("contractto_Year_ID")))
			.selectByVisibleText(hotelContract.getContractTo().split(
					"-")[2]);

    		new Select(driver.findElement(By.id("contractto_Month_ID")))
			.selectByVisibleText(hotelContract.getContractTo().split(
					"-")[1]);
	
			new Select(driver.findElement(By.id("contractto_Day_ID")))
					.selectByVisibleText(hotelContract.getContractTo().split(
							"-")[0]);
	

			// Select the Inventory Type

			if (hotelContract.getInventoryType() == HotelInventoryType.FREESELL) {
				driver.findElement(By.id("inventorytype_F")).click();

			} else if (hotelContract.getInventoryType() == HotelInventoryType.ALLOTMENTS) {
				driver.findElement(By.id("inventorytype_A")).click();
			} else if (hotelContract.getInventoryType() == HotelInventoryType.ONREQUEST) {
				driver.findElement(By.id("inventorytype_R")).click();
			}

			// Alternative Options Search Satisfied

			if (hotelContract.getSearchSatistied() == InventoryActionType.FREESELL) {
				driver.findElement(By.id("searchSatisfied_FS")).click();
				driver.findElement(By.id("searchSatisfiedcall_FS")).click();

			}
			if (hotelContract.getSearchSatistied() == InventoryActionType.CONFIRMATION) {
				driver.findElement(By.id("searchSatisfied_CO")).click();
				driver.findElement(By.id("searchSatisfiedcall_CO")).click();

			} else if (hotelContract.getSearchSatistied() == InventoryActionType.CALLTOBOOK) {
				driver.findElement(By.id("searchSatisfied_CL")).click();
			} else if (hotelContract.getSearchSatistied() == InventoryActionType.NONE) {
				driver.findElement(By.id("searchSatisfied_NO")).click();
				driver.findElement(By.id("searchSatisfiedcall_NO")).click();
			} else if (hotelContract.getSearchSatistied() == InventoryActionType.REQUEST) {
				driver.findElement(By.id("searchSatisfied_RQ")).click();
				driver.findElement(By.id("searchSatisfiedcall_RQ")).click();
			}

			// Alternative Options Inventory Exhausted

			if (hotelContract.getInventoryType() == HotelInventoryType.ALLOTMENTS) {

				if (hotelContract.getInventoryExhausted() == InventoryActionType.FREESELL) {
					driver.findElement(By.id("autoswitch_FS")).click();
					driver.findElement(By.id("autoswitchcall_FS")).click();

				} else if (hotelContract.getInventoryExhausted() == InventoryActionType.REQUEST) {
					driver.findElement(By.id("autoswitch_RQ")).click();
					driver.findElement(By.id("autoswitchcall_RQ")).click();
				} else if (hotelContract.getInventoryExhausted() == InventoryActionType.CALLTOBOOK) {
					driver.findElement(By.id("autoswitch_CL")).click();
				} else if (hotelContract.getInventoryExhausted() == InventoryActionType.NONE) {
					driver.findElement(By.id("autoswitch_NO")).click();
					driver.findElement(By.id("autoswitchcall_NO")).click();
				}
			}

			else {
				try {
					driver.manage().timeouts()
							.implicitlyWait(2, TimeUnit.SECONDS);
					driver.findElement(By.id("autoswitch_RQ"));
					logger.fatal("Inventory Exhauseted option Available for freecell or on-request hotels "
							+ CurrentHotel.getHotelName());
					driver.manage().timeouts()
							.implicitlyWait(30, TimeUnit.SECONDS);
				} catch (Exception e) {
					driver.manage().timeouts()
							.implicitlyWait(30, TimeUnit.SECONDS);
				}
			}

			// =============================================================================================================

			if (hotelContract.getInventoryType() == HotelInventoryType.ONREQUEST) {
				logger.info("Hotel is on-request Checking Whether Freesell option not available");
				try {
					driver.manage().timeouts()
							.implicitlyWait(2, TimeUnit.SECONDS);
					driver.findElement(By.id("cutoff_FS"));
					logger.fatal("Hotel is on-request but Freesell option available");

				} catch (Exception e) {
					logger.fatal("Hotel is on-request and Freesell option available");
					takeScreenshot(ScenarioUrl+"/OnrequestFreeCellAvailable.jpg", driver);
				}
			}

			// =============================================================================================================

			// Alternative Options Cutoff Applied

			if (hotelContract.getCutoffApplied() == InventoryActionType.FREESELL) {
				driver.findElement(By.id("cutoff_FS")).click();
				driver.findElement(By.id("cutoffcall_FS")).click();

			} else if (hotelContract.getCutoffApplied() == InventoryActionType.REQUEST) {
				driver.findElement(By.id("cutoff_RQ")).click();
				driver.findElement(By.id("cutoffcall_RQ")).click();
			} else if (hotelContract.getCutoffApplied() == InventoryActionType.CALLTOBOOK) {
				driver.findElement(By.id("cutoff_CL")).click();
			} else if (hotelContract.getCutoffApplied() == InventoryActionType.NONE) {
				driver.findElement(By.id("cutoff_NO")).click();
				driver.findElement(By.id("cutoffcall_NO")).click();
			}

			// Alternative Options Min Night Restriction

			if (hotelContract.getMinNightRestriction() == InventoryActionType.FREESELL) {
				driver.findElement(By.id("minrestriction_FS")).click();
				driver.findElement(By.id("minrestrictioncall_FS")).click();

			} else if (hotelContract.getMinNightRestriction() == InventoryActionType.REQUEST) {
				driver.findElement(By.id("minrestriction_RQ")).click();
				driver.findElement(By.id("minrestrictioncall_RQ")).click();
			} else if (hotelContract.getMinNightRestriction() == InventoryActionType.CALLTOBOOK) {
				driver.findElement(By.id("minrestriction_CL")).click();
			} else if (hotelContract.getMinNightRestriction() == InventoryActionType.NONE) {
				driver.findElement(By.id("minrestriction_NO")).click();
				driver.findElement(By.id("minrestrictioncall_NO")).click();
			}

			// Alternative Options Max Night Restriction

			if (hotelContract.getMAxNightRestriction() == InventoryActionType.FREESELL) {
				driver.findElement(By.id("maxrestriction_FS")).click();
				driver.findElement(By.id("maxrestrictioncall_FS")).click();

			} else if (hotelContract.getMAxNightRestriction() == InventoryActionType.REQUEST) {
				driver.findElement(By.id("maxrestriction_RQ")).click();
				driver.findElement(By.id("maxrestrictioncall_RQ")).click();
			} else if (hotelContract.getMAxNightRestriction() == InventoryActionType.CALLTOBOOK) {
				driver.findElement(By.id("maxrestriction_CL")).click();
			} else if (hotelContract.getMAxNightRestriction() == InventoryActionType.NONE) {
				driver.findElement(By.id("maxrestriction_NO")).click();
				driver.findElement(By.id("maxrestrictioncall_NO")).click();
			}
			// Alternative Options Blackout

			if (hotelContract.getBlackout() == InventoryActionType.FREESELL) {
				driver.findElement(By.id("blackout_FS")).click();
				driver.findElement(By.id("blackoutcall_FS")).click();

			} else if (hotelContract.getBlackout() == InventoryActionType.REQUEST) {
				driver.findElement(By.id("blackout_RQ")).click();
				driver.findElement(By.id("blackoutcall_RQ")).click();
			} else if (hotelContract.getBlackout() == InventoryActionType.CALLTOBOOK) {
				driver.findElement(By.id("blackout_CL")).click();
			} else if (hotelContract.getBlackout() == InventoryActionType.NONE) {
				driver.findElement(By.id("blackout_NO")).click();
				driver.findElement(By.id("blackoutcall_NO")).click();
			}

			// No Arrival

			if (hotelContract.getNoArrival() == InventoryActionType.FREESELL) {
				driver.findElement(By.id("noarraival_FS")).click();
				driver.findElement(By.id("noarraivalcall_FS")).click();

			} else if (hotelContract.getNoArrival() == InventoryActionType.REQUEST) {
				driver.findElement(By.id("noarraival_RQ")).click();
				driver.findElement(By.id("noarraivalcall_RQ")).click();
			} else if (hotelContract.getNoArrival() == InventoryActionType.CALLTOBOOK) {
				driver.findElement(By.id("noarraival_CL")).click();
			} else if (hotelContract.getNoArrival() == InventoryActionType.NONE) {
				driver.findElement(By.id("noarraival_NO")).click();
				driver.findElement(By.id("noarraivalcall_NO")).click();
			}

			if (CurrentHotel.getRateContract() == RateContractByType.COMMISSIONABLE) {
				if (hotelContract.getCommType() == ChargeByType.PERCENTAGE)
					driver.findElement(By.id("commissionBasedOn_per")).click();
				else if (hotelContract.getCommType() == ChargeByType.VALUE)
					driver.findElement(By.id("commissionBasedOn_value"))
							.click();

				driver.findElement(By.id("commissionAmount")).clear();
				driver.findElement(By.id("commissionAmount")).sendKeys(
						hotelContract.getCommvalue());
			}
			driver.findElement(
					By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input"))
					.click();

			try {
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				if (driver.findElement(By.id("MainMsgBox_msgDisplayArea"))
						.isDisplayed()) {
					String ErrorText = driver.findElement(
							By.id("MainMsgBox_msgDisplayArea")).getText();
					logger.warn("Contract Not Added---->" + ErrorText);
					takeScreenshot(ScenarioUrl+"/IndividualContractNotSaved.jpg", driver);
				} else {
					logger.warn("Individual Contract Added Successfully");
				}
			} catch (Exception e) {
				try {
					if (driver.findElement(By.id("dialogMsgText"))
							.isDisplayed()) {
						String ErrorText = driver.findElement(
								By.id("dialogMsgText")).getText();
						// System.out.println("Contract Not Added---->"+
						// ErrorText);
						logger.fatal(CurrentHotel.getHotelName()
								+ "  Contract  not added successfully   --->"
								+ ErrorText);
						// driver.findElement(By.id("dialogMsgText")).getText();
						takeScreenshot(ScenarioUrl+"/ContractNotSaved.jpg", driver);
						driver.findElement(By.id("dialogMsgActionButtons"))
								.click();
					} else {
						logger.info(CurrentHotel.getHotelName()
								+ "  Contract  added successfully");
					}
				} catch (Exception e2) {
					logger.info(CurrentHotel.getHotelName()
							+ "  Contract  added successfully");
				}
			}
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			logger.info("Saving Contract Page");
			driver.switchTo().defaultContent();
			driver.findElement(By.name("submit")).click();
			driver.switchTo().defaultContent();

		}

		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By
					.id("dialogMsgText")));
			System.out.println(driver.findElement(By.id("dialogMsgText"))
					.isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText"))
						.getText();
				// System.out.println(TextMessage);
				logger.info(CurrentHotel.getHotelName()
						+ " Contract Page Saving Status - DONE  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
						.click();
			} else {

				String TextMessage = driver.findElement(
						By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				logger.fatal(CurrentHotel.getHotelName()
						+ " Contract Page  Saving Status - Failed  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
						.click();
				takeScreenshot(ScenarioUrl+"/ContractPageNotSaved.jpg", driver);
			}

		} catch (Exception e) {
			logger.fatal(CurrentHotel.getHotelName()
					+ " Contract Saving Status - Failed  Message Out :----->"
					+ e.toString());
			System.out.println(e.toString());
			takeScreenshot(ScenarioUrl+"/ContractPageNotSaved.jpg", driver);

		}

		return driver;

	}

	public WebDriver AmenitiesPage(WebDriver driver)
			throws InterruptedException {

		logger.info("Starting Setting Up Amenities");
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupAmenitiesPage.do");

		try {
			driver.findElement(By.id("hotelname")).clear();
			driver.findElement(By.id("hotelname")).sendKeys(
					CurrentHotel.getHotelName());
			driver.findElement(By.id("hotelname_lkup")).click();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			new WebDriverWait(driver, 60);
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			logger.fatal("Amenity Page Hotel Seclection Failed..!! "+ e.getMessage());
			takeScreenshot(ScenarioUrl+"/AmenitiesPageFailed.jpg", driver);
			return driver;
		}

		try {
			driver.findElement(By.id("AmenityId_1")).click();
			driver.findElement(By.id("AmenityId_4")).click();
			driver.findElement(By.id("AmenityId_6")).click();
			driver.findElement(By.id("AmenityId_8")).click();
			driver.findElement(By.id("AmenityId_9")).click();
		} catch (Exception e) {
			logger.fatal("Amenities selection Failed"+ e.getMessage());
			takeScreenshot(ScenarioUrl+"/AmenitiesSelectionFailed.jpg", driver);
			e.printStackTrace();
		}
        
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		logger.info("Saving Contract Page");
		driver.switchTo().defaultContent();
		driver.findElement(By.name("submit")).click();
		driver.switchTo().defaultContent();
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By
					.id("dialogMsgText")));
			System.out.println(driver.findElement(By.id("dialogMsgText"))
					.isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText"))
						.getText();
				// System.out.println(TextMessage);
				logger.info(CurrentHotel.getHotelName()
						+ " Amenity Page Saving Status - DONE  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
						.click();
			} else {

				String TextMessage = driver.findElement(
						By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				logger.fatal(CurrentHotel.getHotelName()
						+ " Amenity Page  Saving Status - Failed  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
						.click();
				takeScreenshot(ScenarioUrl+"/AmenityPageNotSaved.jpg", driver);
			}

		} catch (Exception e) {
			logger.fatal(CurrentHotel.getHotelName()
					+ " Amenity Saving Status - Failed  Message Out :----->"
					+ e.toString());
			System.out.println(e.toString());
			takeScreenshot(ScenarioUrl+"/AmenityPageNotSaved.jpg", driver);

		}

		return driver;


		// ***************************************************************************************
	}

	
	
	public WebDriver FiltersPage(WebDriver driver) throws InterruptedException {
        
		ArrayList<String> Filters = new ArrayList<String>(Arrays.asList(CurrentHotel.getFilter().split("/")));
		  logger.info(CurrentHotel.getHotelName()
		  +"Number Of Filters to be selected->"+Filters.size());
		  
		logger.info("Starting Setting Up Filter");
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupFiltersPage.do");

		try {
			driver.findElement(By.id("hotelname")).clear();
			driver.findElement(By.id("hotelname")).sendKeys(
					CurrentHotel.getHotelName());
			driver.findElement(By.id("hotelname_lkup")).click();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("HotelTypeId_1")));
		    	
		
		} catch (Exception e) {
			logger.fatal("Filters Page Hotel Seclection Failed or Hotel Properties not available!! "+ e.getMessage());
			takeScreenshot(ScenarioUrl+"/FiltersPageFailed.jpg", driver);
			return driver;
		}

		HashMap<String, WebElement>  EleList = new HashMap<String, WebElement>();
		try {
			ArrayList<WebElement> PropertyList   = new ArrayList<WebElement>(driver.findElements(By.id("elementlabel_description")));
		   
			for (Iterator<WebElement> iterator = PropertyList.iterator(); iterator
					.hasNext();) {
				WebElement webElement = (WebElement) iterator.next();
				String name = webElement.getText();
				EleList.put(name, driver.findElement(By.id(webElement.getAttribute("for"))));

			}
		} catch (Exception e) {
			logger.fatal("Hotel is not -Exception occured when getting property elements -->" + e.toString());
		}
		  
		  Iterator<String> iterator = Filters.iterator();
		  
		  while (iterator.hasNext()){
		   EleList.get(iterator.next().trim()).click();  
		 }
		 

		return driver;

		// ***************************************************************************************
	}

	public WebDriver ContentPage(WebDriver driver) throws InterruptedException {

		logger.info("Starting Setting Up Content");
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupContentPage.do");

		try {
			driver.findElement(By.id("hotelname")).clear();
			driver.findElement(By.id("hotelname")).sendKeys(
					CurrentHotel.getHotelName());
			driver.findElement(By.id("hotelname_lkup")).click();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			new WebDriverWait(driver, 60);
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			logger.fatal("Content Page Hotel Seclection Failed..!! "+ e.getMessage());
			takeScreenshot(ScenarioUrl+"/ContentPageFailed.jpg", driver);
            return driver;
		}

		// Short Description

		try {
			driver.findElement(
					By.xpath(".//*[@id='scrollerAllID']/table[2]/tbody/tr[1]/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img"))
					.click();

			driver.switchTo().frame("texteditor");
			driver.switchTo().frame("mce_editor_0");

			logger.info("default value----->"+driver.findElement(By.xpath("/html")).getAttribute("value"));
			driver.findElement(By.xpath("/html")).sendKeys(
					CurrentHotel.getShortDescription());
			driver.switchTo().defaultContent();
			driver.switchTo().frame("texteditor");
			driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[2]/img"))
					.click();
			
		} catch (Exception e) {
			logger.fatal("Content Page- Issue when adding short description"+ e.getMessage());
			takeScreenshot(ScenarioUrl+"/ContentShortDescription.jpg", driver);
			e.printStackTrace();
		}

		// Detailed Description

		try {
			driver.switchTo().defaultContent();
			driver.findElement(
					By.xpath(".//*[@id='scrollerAllID']/table[2]/tbody/tr[1]/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[3]/a/img"))
					.click();
			driver.switchTo().frame("texteditor");
			driver.switchTo().frame("mce_editor_0");

			driver.findElement(By.xpath("/html")).sendKeys(
					CurrentHotel.getLongDescription());
			driver.switchTo().defaultContent();
			driver.switchTo().frame("texteditor");
			driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[2]/img"))
					.click();
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			logger.fatal("Content Page- Issue when adding short description"+ e.getMessage());
			takeScreenshot(ScenarioUrl+"/ContentLongDescription.jpg", driver);
			e.printStackTrace();
		}

		// Latitude

		try {
			driver.findElement(By.id("latitude")).sendKeys(
					CurrentHotel.getLatitude());
			// Longitude
			driver.findElement(By.id("longitude")).sendKeys(
					CurrentHotel.getLongitude());
		} catch (Exception e) {
			logger.fatal("Content Page- Issue when adding short cordinates"+ e.getMessage());
			takeScreenshot(ScenarioUrl+"/ContentLongDescription.jpg", driver);
			e.printStackTrace();
		}

		driver.findElement(By.name("submit")).click();
		((JavascriptExecutor) driver)
				.executeScript("javascript:closeDialogMsg(dialogMsgBox);");

		return driver;

		// ***************************************************************************************
	}

	public WebDriver ImagesPage(WebDriver driver) throws InterruptedException {

		logger.info("Starting Setting Up Images");
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupImagesPage.do");

		try {
			driver.findElement(By.id("hotelname")).clear();
			driver.findElement(By.id("hotelname")).sendKeys(
					CurrentHotel.getHotelName());
			driver.findElement(By.id("hotelname_lkup")).click();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			new WebDriverWait(driver, 60);
			driver.switchTo().defaultContent();

			// Image Uploader
			for (int i = 1; i < 6; i++) {

				((JavascriptExecutor) driver)
						.executeScript("($('.standalonelink')).click()");
				driver.switchTo().frame("dialogwindow");

			try {
				if (i == 1)
					driver.findElement(By.id("thumbnailimage_Y")).click();
				else
					driver.findElement(By.id("caption")).sendKeys(" "+i);
			} catch (Exception e) {
				// TODO: handle exception
			}
				
				String FullPath = new File(CurrentHotel.getPath()).getAbsolutePath();
				driver.findElement(By.id("theFile")).sendKeys(
						FullPath + System.getProperty("file.separator") + i + ".jpg");
				
				driver.findElement(By.id("saveButId")).click();
				((JavascriptExecutor) driver)
						.executeScript("javascript:close_reload();");
				driver.switchTo().defaultContent();

			}
			
			

		} catch (Exception e) {
			// TODO: handle exception
		}
		
	//	driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
		return driver;

		// ***************************************************************************************
	}

	public WebDriver policyPage(WebDriver driver) throws InterruptedException {
		logger.info("Starting Setting Up Policies");
		driver.switchTo().defaultContent();
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupPoliciesPage.do");
		
		try {
			driver.findElement(By.id("hotelname")).clear();
			driver.findElement(By.id("hotelname")).sendKeys(
					CurrentHotel.getHotelName());
			driver.findElement(By.id("hotelname_lkup")).click();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			new WebDriverWait(driver, 60);
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (CurrentHotel.isChildrenAllowed())
			driver.findElement(By.id("childrenallowed_Y")).click();
		else
			driver.findElement(By.id("childrenallowed_N")).click();

		new Select(driver.findElement(By.name("freechildagefrom")))
				.selectByValue(CurrentHotel.getChargesFreeAgeFrom());
		new Select(driver.findElement(By.name("freechildageto")))
				.selectByValue(CurrentHotel.getChargesFreeAgeTo());

		new Select(driver.findElement(By.name("childrateagefrom")))
				.selectByValue(CurrentHotel.getChildRateApplicableFrom());
		new Select(driver.findElement(By.name("childrateageto")))
				.selectByValue(CurrentHotel.getChildRateApplicableTo());

		new Select(driver.findElement(By.name("checkin")))
				.selectByVisibleText(CurrentHotel.getStandardCheckIn());
		new Select(driver.findElement(By.name("checkout")))
				.selectByVisibleText(CurrentHotel.getStandardCheckOut());

		// Add CC

		ArrayList<HotelPolicy> policies = CurrentHotel.getHotelPolicies();
		logger.info(CurrentHotel.getHotelName() + "Number Of Policies->"
				+ policies.size());
		// driver.switchTo().frame(driver.findElement(By.id("hotelcancellationdetails")));
		for (Iterator<HotelPolicy> iterator = 	policies.iterator(); iterator
				.hasNext();) {
			HotelPolicy hotelPolicy = (HotelPolicy) iterator.next();

			logger.info("Adding Policy -->" + hotelPolicy);
			driver.switchTo().defaultContent();	
			driver.switchTo().frame(
					driver.findElement(By.id("hotelcancellationdetails")));
			driver.findElement(By.id("add_can_policy")).click();

			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("dialogwindow")));

			
			 new Select(driver.findElement(By.id("policyfrom_Year_ID")))
				.selectByVisibleText(hotelPolicy.getFrom().split("-")[2]);
			new Select(driver.findElement(By.id("policyfrom_Month_ID")))
			.selectByVisibleText(hotelPolicy.getFrom().split("-")[1]);
	        new Select(driver.findElement(By.id("policyfrom_Day_ID")))
					.selectByVisibleText(hotelPolicy.getFrom().split("-")[0]);
			

	        new Select(driver.findElement(By.id("policyto_Year_ID")))
			.selectByVisibleText(hotelPolicy.getTo().split("-")[2]);
			new Select(driver.findElement(By.id("policyto_Month_ID")))
			.selectByVisibleText(hotelPolicy.getTo().split("-")[1]);
	        new Select(driver.findElement(By.id("policyto_Day_ID")))
					.selectByVisibleText(hotelPolicy.getTo().split("-")[0]);
	

			// driver.findElement(By.id("cancellationfeebasedon_nonights")).click();
			driver.findElement(By.id("applicablearrivalpriod")).clear();

			driver.findElement(By.id("applicablearrivalpriod")).sendKeys(
					"0" + hotelPolicy.getArrivalLessThan());

			driver.findElement(By.id("cancellationbuffer")).clear();
			driver.findElement(By.id("cancellationbuffer")).sendKeys(
					"0" + hotelPolicy.getCancellatrionBuffer());

			if (hotelPolicy.getStdChargeByType() == ChargeByType.NIGHTRATE)
				driver.findElement(By.id("cancellationfeebasedon_nonights"))
						.click();
			else if (hotelPolicy.getStdChargeByType() == ChargeByType.PERCENTAGE)
				driver.findElement(By.id("cancellationfeebasedon_per")).click();
			else if (hotelPolicy.getStdChargeByType() == ChargeByType.VALUE)
				driver.findElement(By.id("cancellationfeebasedon_value"))
						.click();

			driver.findElement(By.id("cancellationamount")).sendKeys(
					hotelPolicy.getStdValue());

			if (hotelPolicy.getNoShowChargeByType() == ChargeByType.NIGHTRATE)
				driver.findElement(
						By.id("noshowcancellationfeebasedon_nonights")).click();
			else if (hotelPolicy.getNoShowChargeByType() == ChargeByType.PERCENTAGE)
				driver.findElement(By.id("noshowcancellationfeebasedon_per"))
						.click();
			else if (hotelPolicy.getNoShowChargeByType() == ChargeByType.VALUE)
				driver.findElement(By.id("noshowcancellationfeebasedon_value"))
						.click();

			driver.findElement(By.id("noshowcancellationamount")).sendKeys(
					hotelPolicy.getNoshowValue());

			driver.findElement(
					By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input"))
					.click();
		
			

			try {
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				if (driver.findElement(By.id("MainMsgBox_msgDisplayArea"))
						.isDisplayed()) {
					String ErrorText = driver.findElement(
							By.id("MainMsgBox_msgDisplayArea")).getText();
					logger.warn("Policy Not Added---->" + ErrorText);
				} else {
					logger.info("Policy Added Successfully");
				}
			} catch (Exception e) {
				try {

					String ErrorText = driver.findElement(
							By.id("dialogMsgText")).getText();
					logger.warn("Contract Added Successfully");
					driver.findElement(By.id("dialogMsgText")).getText();
					driver.findElement(By.id("dialogMsgActionButtons")).click();

				} catch (Exception e2) {
					System.out.println("Policy Added Successfully");
				}
			}
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			((JavascriptExecutor) driver)
			.executeScript("javascript:closeDialogMsg(dialogMsgBox);");

		}

	    ArrayList<HotelTax> HotelTaxList = CurrentHotel.getHotelTax();
	    
	    if((HotelTaxList != null) && (HotelTaxList.size() != 0))
	    {
	        for (Iterator<HotelTax> iterator2 = HotelTaxList.iterator(); iterator2
					.hasNext();) {
				HotelTax hotelTax = (HotelTax) iterator2.next();
				driver.switchTo().defaultContent();
				driver.switchTo().frame(
						driver.findElement(By.id("hoteltaxesdetails")));
				driver.findElement(By.className("standalonelink")).click();

				driver.switchTo().defaultContent();
				driver.switchTo().frame(driver.findElement(By.id("dialogwindow")));

				
				new Select(driver.findElement(By.id("taxesfrom_Month_ID")))
				.selectByVisibleText(hotelTax.getFromT().split("-")[1]);
		        new Select(driver.findElement(By.id("taxesfrom_Year_ID")))
				.selectByVisibleText(hotelTax.getFromT().split("-")[2]);
				new Select(driver.findElement(By.id("taxesfrom_Day_ID")))
						.selectByVisibleText(hotelTax.getFromT().split("-")[0]);
				

				
				new Select(driver.findElement(By.id("taxesto_Month_ID")))
				.selectByVisibleText(hotelTax.getToT().split("-")[1]);
		        new Select(driver.findElement(By.id("taxesto_Year_ID")))
				.selectByVisibleText(hotelTax.getToT().split("-")[2]);

				new Select(driver.findElement(By.id("taxesto_Day_ID")))
						.selectByVisibleText(hotelTax.getToT().split("-")[0]);
		
				
				 if (hotelTax.getSalesTaxChargeType() == ChargeByType.PERCENTAGE)
					driver.findElement(By.id("salestaxpayment_P")).click();
				else if (hotelTax.getSalesTaxChargeType() == ChargeByType.VALUE)
					driver.findElement(By.id("salestaxpayment_F"))
							.click();
				 driver.findElement(By.id("salestax")).clear();
				 driver.findElement(By.id("salestax")).sendKeys(hotelTax.getSalesTaxValue());


				 if (hotelTax.getOccupancyChargeType() == ChargeByType.PERCENTAGE)
					driver.findElement(By.id("occutaxpayment_P")).click();
				else if (hotelTax.getOccupancyChargeType() == ChargeByType.VALUE)
					driver.findElement(By.id("occutaxpayment_F"))
							.click();
				 driver.findElement(By.id("occutax")).clear();
				 driver.findElement(By.id("occutax")).sendKeys(hotelTax.getOccupancyTaxValue());
				 

				 if (hotelTax.getEnergyChargeType() == ChargeByType.PERCENTAGE)
					driver.findElement(By.id("energytaxpayment_P")).click();
				else if (hotelTax.getEnergyChargeType() == ChargeByType.VALUE)
					driver.findElement(By.id("energytaxpayment_F"))
							.click();
				 driver.findElement(By.id("energytax")).clear();
				 driver.findElement(By.id("energytax")).sendKeys(hotelTax.getEnergyTaxValue());

				driver.findElement(By.id("miscfees")).clear(); 
				driver.findElement(By.id("miscfees")).sendKeys(
						hotelTax.getMiscellaneousFees());

				driver.findElement(
						By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input"))
						.click();

				try {
					driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
					if (driver.findElement(By.id("MainMsgBox_msgDisplayArea"))
							.isDisplayed()) {
						String ErrorText = driver.findElement(
								By.id("MainMsgBox_msgDisplayArea")).getText();
						logger.warn("Contract Not Added---->" + ErrorText);
					} else {
						logger.info("Contract Added Successfully");
					}
				} catch (Exception e) {
					try {

						String ErrorText = driver.findElement(
								By.id("dialogMsgText")).getText();
						logger.warn("Contract Added Successfully");
						driver.findElement(By.id("dialogMsgText")).getText();
						driver.findElement(By.id("dialogMsgActionButtons")).click();

					} catch (Exception e2) {
						System.out.println("Contract Added Successfully");
					}
				
			    }
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.switchTo().defaultContent();
				((JavascriptExecutor) driver)
				.executeScript("javascript:closeDialogMsg(dialogMsgBox);");
		    }
	    }
	    
	
		
	    
	    driver.switchTo().defaultContent();
		logger.info("Clicking On Save Button");
		driver.findElement(By.name("submit")).click();

		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By
					.id("dialogMsgText")));
			System.out.println(driver.findElement(By.id("dialogMsgText"))
					.isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText"))
						.getText();
				System.out.println(TextMessage);
				logger.info(CurrentHotel.getHotelName()
						+ " Policy/Tax Saving Status - DONE  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
						.click();
			} else {

				String TextMessage = driver.findElement(
						By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				logger.fatal(CurrentHotel.getHotelName()
						+ " Contract/Tax Saving Status - Failed  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
						.click();
			}

		} catch (Exception e) {
			logger.fatal(CurrentHotel.getHotelName()
					+ " Contract/Tax Saving Status - Failed  Message Out :----->"
					+ e.toString());
			System.out.println(e.toString());

		}

		return driver;
	}

	public WebDriver OccupancyPage(WebDriver driver)
			throws InterruptedException {

		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/AssignRoomOccupancyPage.do?module=contract");
		driver.navigate().refresh();
		driver.findElement(By.id("hotelname")).clear();
		driver.findElement(By.id("hotelname")).sendKeys(
				CurrentHotel.getHotelName());
		driver.findElement(By.id("hotelname_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();

		driver.switchTo().defaultContent();

		Map<String, HotelRoom> RoomList = CurrentHotel.getHotelRooms();

		ArrayList<HotelRoom> rooms = new ArrayList<HotelRoom>(RoomList.values());

		for (Iterator<HotelRoom> iterator = rooms.iterator(); iterator.hasNext();) {
			HotelRoom hotelRoom = (HotelRoom) iterator.next();

			driver.findElement(
					By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/table/tbody/tr/td[2]"))
					.click();
			

			driver.switchTo().frame(driver.findElement(By.id("dialogwindow")));
			driver.findElement(By.id("roomType")).sendKeys(
					hotelRoom.getRoomType());
			driver.findElement(By.id("roomType_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("dialogwindow")));
			driver.findElement(By.id("bedType")).sendKeys(
					hotelRoom.getBedType());
			driver.findElement(By.id("bedType_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("dialogwindow")));
			driver.findElement(By.id("ratePlan")).sendKeys(
					hotelRoom.getRatePlan());
			driver.findElement(By.id("ratePlan_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame(driver.findElement(By.id("dialogwindow")));
			driver.findElement(By.id("mstandardadults")).clear();
			driver.findElement(By.id("madditionaladults")).clear();
			driver.findElement(By.id("mchildren")).clear();
			driver.findElement(By.id("mstandardadults")).sendKeys(
					hotelRoom.getStdAdults());
			driver.findElement(By.id("madditionaladults")).sendKeys(
					hotelRoom.getAAdults());
			driver.findElement(By.id("mchildren")).sendKeys(
					hotelRoom.getChildren());
			driver.findElement(By.id("saveButId")).click();

			try {
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

				if (driver.findElement(By.id("MainMsgBox_msgDisplayArea"))
						.isDisplayed()) {
					logger.info("MainMsgBox_msgDisplayArea is displayed");
					String ErrorText = driver.findElement(
							By.id("MainMsgBox_msgDisplayArea")).getText();
					logger.fatal("Room Occupancy Not Added---->" + ErrorText);
					driver.findElement(
							By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
							.click();
					driver.findElement(
							By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img"))
							.click();
					driver.switchTo().defaultContent();
				} else if (driver.findElement(By.id("dialogMsgText"))
						.isDisplayed()) {
					logger.info("dialogMsgText displayed");
					String ErrorText = driver.findElement(
							By.id("dialogMsgText")).getText();
					logger.warn("Room Occupancy Not Added---->" + ErrorText);
					driver.findElement(By.id("dialogMsgText")).getText();
					driver.findElement(By.id("dialogMsgActionButtons")).click();
					driver.switchTo().defaultContent();
				} else {
					logger.info(CurrentHotel.getHotelName() + " , "
							+ hotelRoom.getRoomType()
							+ "Room Occupancy Added Successfully");
				}
			} catch (Exception e) {
				try {

					String ErrorText = driver.findElement(
							By.id("dialogMsgText")).getText();
					logger.fatal("Room Occupancy Not Added---->" + ErrorText);
					driver.findElement(By.id("dialogMsgText")).getText();
					driver.findElement(By.id("dialogMsgActionButtons")).click();

				} catch (Exception e2) {
					logger.info(CurrentHotel.getHotelName() + " , "
							+ hotelRoom.getRoomType()
							+ "Room Occupancy Added Successfully");
				}
			}

			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}
		return driver;
	}

	// ****Record Already Exists********

	// Inventory Setup Page

	public WebDriver addInventory(WebDriver driver) throws InterruptedException {
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/inventoryandrates/InventorySetupPeriodBasisPage.do?module=contract");
		driver.switchTo().defaultContent();
		/*
		 * try { driver.navigate().refresh();
		 * driver.findElement(By.id("hotelname")).clear();
		 * driver.findElement(By.
		 * id("hotelname")).sendKeys(CurrentHotel.getHotelName());
		 * driver.findElement(By.id("hotelname_lkup")).click();
		 * driver.switchTo().frame("lookup"); driver.findElement(
		 * By.xpath(".//*[@id='row-0']/td")).click(); new WebDriverWait(driver,
		 * 60); driver.switchTo().defaultContent(); } catch (Exception e) { //
		 * TODO: handle exception }
		 */

		new Select(driver.findElement(By.id("periodFrom_Year_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractFromDate()
				.split("-")[2]);
		new Select(driver.findElement(By.id("periodFrom_Month_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractFromDate()
				.split("-")[1]);
        new Select(driver.findElement(By.id("periodFrom_Day_ID")))
				.selectByVisibleText(CurrentHotel.getHotelContractFromDate()
						.split("-")[0]);
		

		
        new Select(driver.findElement(By.id("periodTo_Month_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractToDate()
				.split("-")[1]);
        new Select(driver.findElement(By.id("periodTo_Year_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractToDate()
				.split("-")[2]);  
        new Select(driver.findElement(By.id("periodTo_Day_ID")))
				.selectByVisibleText(CurrentHotel.getHotelContractToDate()
						.split("-")[0]);
		

		try {
			driver.findElement(
					By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img"))
					.click();
		} catch (Exception e) {
			((JavascriptExecutor) driver)
					.executeScript("javascript: viewData(1);");
		}

		// TODO:Handle Messages
		try {
			if (driver.findElement(By.id("MainMsgBox_msgDisplayArea"))
					.isDisplayed()) {
				String ErrorText = driver.findElement(
						By.id("MainMsgBox_msgDisplayArea")).getText();
				logger.fatal(CurrentHotel.getHotelName()
						+ " Inventory viewing Failed :----->" + ErrorText);
			}
		} catch (Exception e) {
			try {
				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String ErrorText = driver.findElement(
							By.id("dialogMsgText")).getText();
					logger.fatal(CurrentHotel.getHotelName()
							+ "Inventory viewing Failed :----->" + ErrorText);
					driver.findElement(By.id("dialogMsgText")).getText();
					driver.findElement(By.id("dialogMsgActionButtons")).click();
				}

			} catch (Exception e2) {
				logger.fatal(CurrentHotel.getHotelName()
						+ " Inventory viewing Success");
			}
		}

	Map<String, HotelRoom> RoomList = CurrentHotel.getHotelRooms();

		if (CurrentHotel.getInventoryObtainedType() == InventoryObtainedByType.BYHOTEL) {
			HotelRoom CurrentRoom = RoomList.values().iterator().next();

			try {
				try {
					for (int i = 0; i < 7; i++) {
						driver.findElement(By.id("totRoomsArray[" + i + "]"))
								.clear();
						driver.findElement(By.id("totRoomsArray[" + i + "]"))
								.sendKeys(CurrentRoom.getTotalRooms());

					}
				} catch (Exception e) {
					logger.warn("total rooms array not available may be freesell or onrequest----->"
							+ CurrentHotel.getHotelName());
					e.printStackTrace();
				}
				
				////////////////////////////////////////////////////////////
				for (int i = 0; i < 7; i++) {
					driver.findElement(By.id("minNightsArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("minNightsArray[" + i + "]"))
							.sendKeys(CurrentRoom.getMinimumNightsStay());
				}
				
				for (int i = 0; i < 7; i++) {
					driver.findElement(By.id("maxNightsArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("maxNightsArray[" + i + "]"))
							.sendKeys(CurrentRoom.getMaximumNightsStay());
				}
				
				for (int i = 0; i < 7; i++) {
					driver.findElement(By.id("cutOffArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("cutOffArray[" + i + "]"))
							.sendKeys(CurrentRoom.getCutOff());
				}
				
			/////////////////////////////////////////////////////////////////

			} catch (Exception e) {
				logger.fatal("Exception when antering inventory data----->"
						+ CurrentHotel.getHotelName());
			}

			try {
				driver.findElement(
						By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img"))
						.click();
			} catch (Exception e) {
				((JavascriptExecutor) driver)
						.executeScript("javascript: saveData(1);");
			}

			try {
				if(driver.findElement(By.id("MainMsgBox_msgDisplayArea")).isDisplayed()){
				String ErrorText = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				logger.warn("Inventory Not Added---->" + ErrorText);
				}
				else{
				String ErrorText = driver.findElement(By.id("dialogMsgText")).getText();
				logger.warn("Inventory  Added 	successfully---->" + ErrorText);
					
				}
			} catch (Exception e) {
				try {

					String ErrorText = driver.findElement(
							By.id("dialogMsgText")).getText();
					logger.warn("Inventory Not Added---->" + ErrorText);
					driver.findElement(By.id("dialogMsgText")).getText();
					driver.findElement(By.id("dialogMsgActionButtons")).click();

				} catch (Exception e2) {
					logger.info("Inventory Added Successfully");
				}
			}

		}

		if (CurrentHotel.getInventoryObtainedType() == InventoryObtainedByType.BYROOM) {
			driver.findElements(By.name("roomBed")).get(0).click();
			List<WebElement> list = driver.findElements(By.name("roomBed"));

			/*
			 * for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			 * WebElement webElement = (WebElement) iterator.next();
			 */
			for (int j = 0; j < list.size(); j++) {

				WebElement webElement = (WebElement) list.get(j);
				String Value = webElement.getAttribute("value");
				webElement.click();
				WebDriverWait wait = new WebDriverWait(driver, 30);
				// System.out.println(webElement.getAttribute("value"));

				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By
							.id("totRoomsArray[0]")));
					String Room = driver.findElement(
							By.id("label_td_roomBed_" + Value)).getText();
					HotelRoom CurrentRoom = RoomList.get(Room.trim());

					for (int i = 0; i < 7; i++) {
						driver.findElement(By.id("totRoomsArray[" + i + "]"))
								.clear();
						driver.findElement(By.id("totRoomsArray[" + i + "]"))
								.sendKeys(CurrentRoom.getTotalRooms());
					}
					
					//////////////////////////////////////////////
					for (int i = 0; i < 7; i++) {
						driver.findElement(By.id("minNightsArray[" + i + "]"))
								.clear();
						driver.findElement(By.id("minNightsArray[" + i + "]"))
								.sendKeys(CurrentRoom.getMinimumNightsStay());
					}
					
					for (int i = 0; i < 7; i++) {
						driver.findElement(By.id("maxNightsArray[" + i + "]"))
								.clear();
						driver.findElement(By.id("maxNightsArray[" + i + "]"))
								.sendKeys(CurrentRoom.getMaximumNightsStay());
					}
					
					for (int i = 0; i < 7; i++) {
						driver.findElement(By.id("cutOffArray[" + i + "]"))
								.clear();
						driver.findElement(By.id("cutOffArray[" + i + "]"))
								.sendKeys(CurrentRoom.getCutOff());
					}
					
				//////////////////////////////////////////////
					
				} catch (Exception e) {
					logger.warn("total rooms array not available may be freesell or onrequest----->"
							+ CurrentHotel.getHotelName());

				}
				try {
					driver.findElement(
							By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img"))
							.click();
				} catch (Exception e) {
					((JavascriptExecutor) driver)
							.executeScript("javascript: saveData(1);");
				}

				try {
					if(driver.findElement(By.id("MainMsgBox_msgDisplayArea")).isDisplayed()){
						String ErrorText = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						logger.warn("Inventory Not Added---->" + ErrorText);
						}
						else{
						String ErrorText = driver.findElement(By.id("dialogMsgText")).getText();
						logger.warn("Inventory  Added 	successfully---->" + ErrorText);
						}
				} catch (Exception e) {
					try {

						String ErrorText = driver.findElement(
								By.id("dialogMsgText")).getText();
						System.out.println("Inventory Not Added---->"
								+ ErrorText);
						driver.findElement(By.id("dialogMsgText")).getText();
						driver.findElement(By.id("dialogMsgActionButtons"))
								.click();

					} catch (Exception e2) {
						System.out.println("Inventory Added Successfully");
					}
				}
				list = driver.findElements(By.name("roomBed"));
			}

		}

		if (CurrentHotel.getInventoryObtainedType() == InventoryObtainedByType.BYROOMBED) {
			driver.findElements(By.name("roomBed")).get(0).click();
			List<WebElement> list = driver.findElements(By.name("roomBed"));

			for (int j = 0; j < list.size(); j++) {

				WebElement webElement = (WebElement) list.get(j);
				String Value = webElement.getAttribute("value");
				webElement.click();
				WebDriverWait wait = new WebDriverWait(driver, 30);
				// System.out.println(webElement.getAttribute("value"));
				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id("totRoomsArray[0]")));
				String Room = driver.findElement(
						By.id("label_td_roomBed_" + Value)).getText();
				HotelRoom CurrentRoom = RoomList.get(Room.trim());

				for (int i = 0; i < 7; i++) {
					driver.findElement(By.id("totRoomsArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("totRoomsArray[" + i + "]"))
							.sendKeys(CurrentRoom.getTotalRooms());
				}
				
				//////////////////////////////////////////////
				for (int i = 0; i < 7; i++) {
					driver.findElement(By.id("minNightsArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("minNightsArray[" + i + "]"))
							.sendKeys(CurrentRoom.getMinimumNightsStay());
				}
				
				for (int i = 0; i < 7; i++) {
					driver.findElement(By.id("maxNightsArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("maxNightsArray[" + i + "]"))
							.sendKeys(CurrentRoom.getMaximumNightsStay());
				}
				
				for (int i = 0; i < 7; i++) {
					driver.findElement(By.id("cutOffArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("cutOffArray[" + i + "]"))
							.sendKeys(CurrentRoom.getCutOff());
				}
				
			//////////////////////////////////////////////
				try {
					driver.findElement(
							By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img"))
							.click();
				} catch (Exception e) {
					((JavascriptExecutor) driver)
							.executeScript("javascript: saveData(1);");
				}

				try {
					if(driver.findElement(By.id("MainMsgBox_msgDisplayArea")).isDisplayed()){
						String ErrorText = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						logger.warn("Inventory Not Added---->" + ErrorText);
						}
						else{
						String ErrorText = driver.findElement(By.id("dialogMsgText")).getText();
						logger.warn("Inventory  Added 	successfully---->" + ErrorText);
						}
				} catch (Exception e) {
					try {

						String ErrorText = driver.findElement(
								By.id("dialogMsgText")).getText();
						System.out.println("Contract Not Added---->"
								+ ErrorText);
						driver.findElement(By.id("dialogMsgText")).getText();
						driver.findElement(By.id("dialogMsgActionButtons"))
								.click();

					} catch (Exception e2) {
						System.out.println("Contract Added Successfully");
					}
				}
				list = driver.findElements(By.name("roomBed"));
			}

		}

		return driver;
	}

	// Rates SetUp page

	public WebDriver addRates(WebDriver driver) throws InterruptedException {

		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/inventoryandrates/RatesSetupPeriodBasisPage.do");

		/*
		 * try { driver.findElement(By.id("hotelname")).clear();
		 * driver.findElement
		 * (By.id("hotelname")).sendKeys(CurrentHotel.getHotelName());
		 * driver.findElement(By.id("hotelname_lkup")).click();
		 * driver.switchTo().frame("lookup"); driver.findElement(
		 * By.xpath(".//*[@id='row-0']/td")).click(); new WebDriverWait(driver,
		 * 60); driver.switchTo().defaultContent(); } catch (Exception e) { //
		 * TODO: handle exception }
		 */

		
		
		new Select(driver.findElement(By.id("periodFrom_Month_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractFromDate()
				.split("-")[1]);
        new Select(driver.findElement(By.id("periodFrom_Year_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractFromDate()
				.split("-")[2]);
		new Select(driver.findElement(By.id("periodFrom_Day_ID")))
				.selectByVisibleText(CurrentHotel.getHotelContractFromDate()
						.split("-")[0]);
		

		
		new Select(driver.findElement(By.id("periodTo_Month_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractToDate()
				.split("-")[1]);
        new Select(driver.findElement(By.id("periodTo_Year_ID")))
		.selectByVisibleText(CurrentHotel.getHotelContractToDate()
				.split("-")[2]);

		new Select(driver.findElement(By.id("periodTo_Day_ID")))
				.selectByVisibleText(CurrentHotel.getHotelContractToDate()
						.split("-")[0]);
	
		try {
			driver.findElement(
					By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img"))
					.click();
		} catch (Exception e) {
			((JavascriptExecutor) driver)
					.executeScript("javascript: viewData(1);");
		}

		// TODO:Handle Messages
		try {
			String ErrorText = driver.findElement(
					By.id("MainMsgBox_msgDisplayArea")).getText();
			logger.fatal("Rate set up page message ---->" + ErrorText);
		} catch (Exception e) {
			try {

				String ErrorText = driver.findElement(By.id("dialogMsgText"))
						.getText();
				logger.fatal("Rate set up page message  ---->" + ErrorText);
				driver.findElement(By.id("dialogMsgText")).getText();
				driver.findElement(By.id("dialogMsgActionButtons")).click();

			} catch (Exception e2) {
				logger.info("Rate Set up page no error message !!");
			}
		}

		// /////////////////////////////////

		Map<String, HotelRoom> RoomList = CurrentHotel.getHotelRooms();
		// ArrayList<HotelRoom> rooms = new
		// ArrayList<HotelRoom>(RoomList.values());
		driver.findElements(By.name("roomBedRatePlan")).get(0).click();
		List<WebElement> list1 = driver
				.findElements(By.name("roomBedRatePlan"));

		for (int j = 0; j < list1.size(); j++) {
			WebElement webElement = (WebElement) list1.get(j);
			String Value = webElement.getAttribute("value");
			webElement.click();
			WebDriverWait wait = new WebDriverWait(driver, 30);
			// System.out.println(webElement.getAttribute("value"));

			if (CurrentHotel.getRateContract() == RateContractByType.NETRATE)
				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id("netRateArray[0]")));
			else if (CurrentHotel.getRateContract() == RateContractByType.COMMISSIONABLE)
				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id("sellRateArray[0]")));

			String Room = driver.findElement(
					By.id("label_td_roomBedRatePlan_" + Value)).getText();
			HotelRoom RateRoom = RoomList.get(Room.split("/")[0].trim());

			for (int i = 0; i < 7; i++) {
				if (CurrentHotel.getRateContract() == RateContractByType.NETRATE) {
					driver.findElement(By.id("netRateArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("netRateArray[" + i + "]"))
							.sendKeys(RateRoom.getNetRate());

					driver.findElement(By.id("adnlAdultNetArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("adnlAdultNetArray[" + i + "]"))
							.sendKeys(RateRoom.getAdditionalAdultRate());

					driver.findElement(By.id("childNetRateArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("childNetRateArray[" + i + "]"))
							.sendKeys(RateRoom.getChildNetRate());

				}

				else if (CurrentHotel.getRateContract() == RateContractByType.COMMISSIONABLE) {
					driver.findElement(By.id("sellRateArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("sellRateArray[" + i + "]"))
							.sendKeys(RateRoom.getNetRate());

					driver.findElement(
							By.id("adnlAdultsellRateArray[" + i + "]")).clear();
					driver.findElement(
							By.id("adnlAdultsellRateArray[" + i + "]"))
							.sendKeys(RateRoom.getAdditionalAdultRate());

					driver.findElement(By.id("childsellRateArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("childsellRateArray[" + i + "]"))
							.sendKeys(RateRoom.getChildNetRate());

				}

			}

			try {
				driver.findElement(
						By.xpath("/html/body/table/tbody/tr[2]/td/div/div/form/table/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img"))
						.click();
			} catch (Exception e) {
				((JavascriptExecutor) driver)
						.executeScript("javascript: saveData(1);");
			}

			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id("dialogMsgText")));
				System.out.println(driver.findElement(By.id("dialogMsgText"))
						.isDisplayed());

				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(
							By.id("dialogMsgText")).getText();
					// System.out.println(TextMessage);
					logger.info(CurrentHotel.getHotelName() + "Room" + RateRoom
							+ "Saving Status - DONE  Message Out :----->"
							+ TextMessage);
					driver.findElement(
							By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
							.click();
				} else {

					String TextMessage = driver.findElement(
							By.id("MainMsgBox_msgDisplayArea")).getText();
					System.out.println(TextMessage);
					logger.fatal(CurrentHotel.getHotelName() + "Room"
							+ RateRoom
							+ "Saving Status - Failed  Message Out :----->"
							+ TextMessage);
					driver.findElement(
							By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
							.click();
				}

			} catch (Exception e) {
				logger.fatal(CurrentHotel.getHotelName() + "Room" + RateRoom
						+ "Saving Status - Failed  Message Out :----->"
						+ e.toString());
				System.out.println(e.toString());

			}

			list1 = driver.findElements(By.name("roomBedRatePlan"));
		}
		return driver;

	}

	public WebDriver ProfiMarkUpPage(WebDriver driver)
			throws InterruptedException {

		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/inventoryandrates/ProfitMarkupPeriodBasisPage.do?module=contract");

		// Booking Channel

		ArrayList<ProfitMarkup> MarkUps = CurrentHotel.getHotelProfitMarkUps();

		for (Iterator<ProfitMarkup> iterator = MarkUps.iterator(); iterator.hasNext();) {
			ProfitMarkup profitMarkup = (ProfitMarkup) iterator.next();

			if (profitMarkup.getBookingChannel().equalsIgnoreCase("Website"))
				driver.findElement(By.id("bookingChannel_WEB")).click();
			else if (profitMarkup.getBookingChannel().equalsIgnoreCase(
					"Call Center"))
				driver.findElement(By.id("bookingChannel_CC")).click();

			if (profitMarkup.getPartnerType()
					.equalsIgnoreCase("Affiliate Name"))
				driver.findElement(By.id("customerType_AFF")).click();
			else if (profitMarkup.getPartnerType().equalsIgnoreCase(
					"Corporate Customer"))
				driver.findElement(By.id("customerType_CO")).click();
			else if (profitMarkup.getPartnerType().equalsIgnoreCase(
					"Tour Operator "))
				driver.findElement(By.id("customerType_TO")).click();

			if (profitMarkup.getChargeType() == ChargeByType.PERCENTAGE)
				driver.findElement(By.id("markupType_PCT")).click();
			else if (profitMarkup.getChargeType() == ChargeByType.VALUE)
				driver.findElement(By.id("markupType_VAL")).click();

			/*
			 * driver.findElement(By.id("rateRegionName")).clear();
			 * driver.findElement
			 * (By.id("rateRegionName")).sendKeys(Repository.Region);
			 * driver.findElement(By.id("rateRegionName_lkup")).click();
			 * driver.switchTo().defaultContent();
			 * driver.switchTo().frame("lookup");
			 * driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			 * driver.switchTo().defaultContent();
			 */

			/*
			 * new Select(driver.findElement(By.id("periodFrom_Day_ID"))).
			 * selectByVisibleText(profitMarkup.getFrom().split("-")[0].trim());
			 * new Select(driver.findElement(By.id("periodFrom_Month_ID"))).
			 * selectByVisibleText(profitMarkup.getFrom().split("-")[1].trim());
			 * new Select(driver.findElement(By.id("periodFrom_Year_ID"))).
			 * selectByVisibleText(profitMarkup.getFrom().split("-")[2].trim());
			 * new Select(driver.findElement(By.id("periodTo_Day_ID"))).
			 * selectByVisibleText(profitMarkup.getTo().split("-")[0].trim());
			 * new Select(driver.findElement(By.id("periodTo_Month_ID"))).
			 * selectByVisibleText(profitMarkup.getTo().split("-")[1].trim());
			 * new Select(driver.findElement(By.id("periodTo_Year_ID"))).
			 * selectByVisibleText(profitMarkup.getTo().split("-")[2].trim());
			 */

			
			new Select(driver.findElement(By.id("periodFrom_Month_ID")))
			.selectByVisibleText(profitMarkup.getFrom().split("-")[1]);
	        new Select(driver.findElement(By.id("periodFrom_Year_ID")))
			.selectByVisibleText(profitMarkup.getFrom().split("-")[2]);
			new Select(driver.findElement(By.id("periodFrom_Day_ID")))
					.selectByVisibleText(profitMarkup.getFrom().split("-")[0]);
		

		
			new Select(driver.findElement(By.id("periodTo_Month_ID")))
			.selectByVisibleText(profitMarkup.getTo()
					.split("-")[1]);
	        new Select(driver.findElement(By.id("periodTo_Year_ID")))
			.selectByVisibleText(profitMarkup.getTo()
					.split("-")[2]);

			new Select(driver.findElement(By.id("periodTo_Day_ID")))
					.selectByVisibleText(profitMarkup.getTo()
							.split("-")[0]);
		
			// Select the Country

			driver.findElement(By.id("countryName")).clear();
			driver.findElement(By.id("countryName")).sendKeys(
					CurrentHotel.getCountry());
			driver.findElement(By.id("countryName_lkup")).click();

			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			driver.switchTo().defaultContent();

			// select the City

			driver.findElement(By.id("cityName")).clear();
			driver.findElement(By.id("cityName")).sendKeys(
					CurrentHotel.getCity());
			driver.findElement(By.id("cityName_lkup")).click();

			driver.switchTo().frame("lookup");
			try {
				driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			} catch (Exception e) {
				driver.findElement(By.xpath(".//*[@id='row-2']/td")).click();
			}
			driver.switchTo().defaultContent();

			// Select the Supplier

			/*
			 * driver.findElement(By.id("supplierName")).clear();
			 * driver.findElement(By.id("supplierName")).sendKeys(
			 * CurrentHotel.getSupplier());
			 * driver.findElement(By.id("supplierName_lkup")).click();
			 * 
			 * driver.switchTo().frame("lookup");
			 * driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			 * 
			 * driver.switchTo().defaultContent();
			 */

			driver.findElement(By.id("hotelMode_H")).click();
			driver.findElement(By.id("viewHotelButton")).click();

			driver.switchTo().frame("profitmarkuphotels");

			boolean isHotelFound = false;

			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

			OuterLoop: for (int i = 1; i < 4; i++) {
				for (int j = 1; j < 4; j++) {
					try {
						String HotelName = driver
								.findElement(
										By.xpath(".//*[@id='idLoadedHotels']/table/tbody/tr["
												+ j
												+ "]/td["
												+ i
												+ "]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]"))
								.getText();
						if (HotelName.trim().equalsIgnoreCase(
								CurrentHotel.getHotelName())) {
							driver.findElement(
									By.xpath(".//*[@id='idLoadedHotels']/table/tbody/tr["
											+ j
											+ "]/td["
											+ i
											+ "]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input"))
									.click();
							isHotelFound = true;
							break OuterLoop;
						}

					} catch (Exception ex) {

					}
				}
			}

			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			
			String[] AdultMarkupArr   = new String[2];
			String[] AAdultMarkupArr  = new String[2];
			String[] ChildMarkupArr   = new String[2];
			//fixMarkupType_sunToThu
			if(profitMarkup.getApplicablePattern().trim().equalsIgnoreCase("Sun to Thu / Fri to Sat")){
				
				        AdultMarkupArr  = profitMarkup.getAdultProfitMarkup().split("/");
						AAdultMarkupArr = profitMarkup.getAdditionalAdultProfitMarkup().split("/");
						ChildMarkupArr  = profitMarkup.getChildProfitMarkup().split("/");
				
				if(!((AdultMarkupArr.length > 1) && (AAdultMarkupArr.length > 1) && (ChildMarkupArr.length > 1)))
				{
			       logger.warn("Profit MarkUp Applicable type 'Sun to Thu / Fri to Sat' but respective value couple not available...");
				}
				driver.findElement(By.id("fixMarkupType_sunToThu")).click();
			}
			else
			{
					for (int i = 0; i < 2; i++) {
						AdultMarkupArr[i]  = profitMarkup.getAdultProfitMarkup() ;
						AAdultMarkupArr[i] = profitMarkup.getAdditionalAdultProfitMarkup() ;
						ChildMarkupArr[i]  = profitMarkup.getChildProfitMarkup();
						
					}
			   driver.findElement(By.id("fixMarkupType_eachDay")).click();
			}

			String AdultMark  = AdultMarkupArr[0].trim();
			String AAdultMark = AAdultMarkupArr[0].trim();
			String ChildMark  = ChildMarkupArr[0].trim();
			
			if (isHotelFound) {
				for (int i = 0; i < 7; i++) {
					
					if(i>4)
					{
						AdultMark   = AdultMarkupArr[1].trim();
				        AAdultMark  = AAdultMarkupArr[1].trim();
					    ChildMark   = ChildMarkupArr[1].trim();
					}
					driver.findElement(By.id("profitMarkupArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("profitMarkupArray[" + i + "]"))
							.sendKeys(AdultMark);

					driver.findElement(By.id("aaprofitMarkupArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("aaprofitMarkupArray[" + i + "]"))
							.sendKeys(AAdultMark);

					driver.findElement(By.id("cprofitMarkupArray[" + i + "]"))
							.clear();
					driver.findElement(By.id("cprofitMarkupArray[" + i + "]"))
							.sendKeys(ChildMark);

				}

				try {
					driver.switchTo().defaultContent();
					driver.findElement(
							By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img"))
							.click();
				} catch (Exception e) {
					((JavascriptExecutor) driver)
							.executeScript("javascript: saveData(1);");
				}

				/*
				 * try { driver.manage().timeouts().implicitlyWait(3,
				 * TimeUnit.SECONDS); String ErrorText =
				 * driver.findElement(By.id
				 * ("MainMsgBox_msgDisplayArea")).getText();
				 * System.out.println("Contract Not Added---->"+ ErrorText); }
				 * catch (Exception e) { try {
				 * 
				 * String ErrorText =
				 * driver.findElement(By.id("dialogMsgText")).getText();
				 * System.out.println("Contract Not Added---->"+ ErrorText);
				 * driver.findElement(By.id("dialogMsgText")).getText();
				 * driver.findElement(By.id("dialogMsgActionButtons")).click();
				 * 
				 * } catch (Exception e2) {
				 * System.out.println("Contract Added Successfully"); } }
				 */

				try {
					new WebDriverWait(driver, 60).until(ExpectedConditions
							.presenceOfElementLocated(By.id("dialogMsgText")));
					System.out.println(driver.findElement(
							By.id("dialogMsgText")).isDisplayed());

					if (driver.findElement(By.id("dialogMsgText"))
							.isDisplayed()) {
						String TextMessage = driver.findElement(
								By.id("dialogMsgText")).getText();
						// System.out.println(TextMessage);
						logger.info(CurrentHotel.getHotelName()
								+ "Profit Markup Saving Status - DONE  Message Out :----->"
								+ TextMessage);
						driver.findElement(
								By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
								.click();
					} else {

						String TextMessage = driver.findElement(
								By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						logger.fatal(CurrentHotel.getHotelName()
								+ "Profit Markup Saving Status - Failed  Message Out :----->"
								+ TextMessage);
						driver.findElement(
								By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
								.click();
					}

				} catch (Exception e) {
					logger.fatal(CurrentHotel.getHotelName()
							+ "Profit Markup Saving Status - Failed  Message Out :----->"
							+ e.toString());
					System.out.println(e.toString());

				}

			}

		}

		return driver;

	}
	
	

	public WebDriver DeleteHotel(WebDriver driver, String HotelName)
			throws InterruptedException {
		driver.get(PropertySet.get("Hotel.BaseUrl")
				+ "/hotels/setup/HotelSetupStandardPage.do?module=contract");
		acceptAlert(driver);

		try {
			driver.findElement(By.id("screenaction_delete")).click();
			logger.info("Navigate to the Hotel deletion page");
		} catch (Exception e) {
			logger.info("Navigation to the Hotel deletion page Failed");
		}

		// Basic Details
		driver.findElement(By.id("hotelname")).sendKeys(HotelName.trim());

		driver.findElement(By.id("hotelname_lkup")).click();
		driver.switchTo().frame(driver.findElement(By.id("lookup")));
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();

		logger.info("Clicking on Save Button");
		driver.findElement(By.name("submit")).click();

		WebDriverWait wait = new WebDriverWait(driver, 30);
		// driver.findElement(By.xpath(".//*[@id='saveButId']")).click();

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By
					.id("dialogMsgText")));
			System.out.println(driver.findElement(By.id("dialogMsgText"))
					.isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText"))
						.getText();
				// System.out.println(TextMessage);
				logger.info(CurrentHotel.getHotelName()
						+ " Hotel Deletion Status - DONE  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
						.click();
			} else {

				String TextMessage = driver.findElement(
						By.id("MainMsgBox_msgDisplayArea")).getText();
				// System.out.println(TextMessage);
				logger.fatal(CurrentHotel.getHotelName()
						+ " Hotel Deletion Status - Failed  Message Out :----->"
						+ TextMessage);
				driver.findElement(
						By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
						.click();
			}

		} catch (Exception e) {
			logger.fatal(CurrentHotel.getHotelName()
					+ "Hotel Deletion Status - Failed  Message Out :----->"
					+ e.toString());
			System.out.println(e.toString());

		}

		return driver;
	}

	
	public WebDriver createSupplementarySetup(WebDriver driver) throws InterruptedException {

		Map<String,HotelSupplementary> supplimentaryList          = CurrentHotel.getHotelSupplimentary();
		Iterator<Map.Entry<String, HotelSupplementary>> entryList = supplimentaryList.entrySet().iterator();
		
		if((supplimentaryList == null) || (supplimentaryList.size() == 0) )
		{
			logger.info(CurrentHotel.getHotelName()+" Supplimentary List is empty----->");
			return driver;
		}
		
		while(entryList.hasNext())
		{
			HotelSupplementary hotelsupplementary = entryList.next().getValue();
			
			driver.get(PropertySet.get("Hotel.BaseUrl")
					+ "/hotels/setup/SupplementaryServicesPage.do?module=contract");
			Thread.sleep(500);

			driver.findElement(By.xpath(".//*[@id='screenaction_create']")).click();
			Thread.sleep(1000);

	        driver.findElement(By.id("supplementaryName")).sendKeys(
					hotelsupplementary.getSupplementary_Name());

			if (!hotelsupplementary.getSupplier_Name().equalsIgnoreCase("N/A")) {
				driver.findElement(By.id("supplierName")).sendKeys(hotelsupplementary.getSupplier_Name().trim());
				driver.findElement(By.id("supplierName_lkup")).click();
				driver.switchTo().frame("lookup");
				try {
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				} catch (Exception e3) {
					logger.fatal("Supplementary Setup ---> Supplier "
							+ hotelsupplementary.getSupplier_Name()
							+ " not available in the lookup");
					takeScreenshot(ScenarioUrl + "/SupplierLookupFaliure.jpg",
							driver);
					e3.printStackTrace();
					return driver;
				}
			}
			
			Thread.sleep(1000);
			driver.switchTo().defaultContent();

			try {
				driver.findElement(By.id("hotelName")).sendKeys(
				hotelsupplementary.getHotel_Name().trim());
				driver.findElement(By.id("hotelName_lkup")).click();
				driver.switchTo().frame("lookup");
				
				if(hotelsupplementary.getHotel_Name().equalsIgnoreCase("ALL"))
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				else
				driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			} catch (Exception e3) {
				logger.fatal("Supplementary Setup ---> HotelName "+hotelsupplementary.getHotel_Name()+" not available in the lookup");
				takeScreenshot(ScenarioUrl+"/HotelNameLookupFaliure.jpg", driver);
				e3.printStackTrace();
				return driver;
				
			}
			
			Thread.sleep(1000);
			driver.switchTo().defaultContent();

			try {
				driver.findElement(By.id("roomTypeName")).clear();
				driver.findElement(By.id("roomTypeName")).sendKeys(
						hotelsupplementary.getRoom_Type());
				driver.findElement(By.id("roomTypeName_lkup")).click();
				driver.switchTo().frame("lookup");
				
				if(hotelsupplementary.getRoom_Type().equalsIgnoreCase("ALL"))
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				else
				driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			} catch (Exception e3) {
				logger.fatal("Supplementary Setup ---> RoomType "+hotelsupplementary.getRoom_Type()+" not available in the lookup");
				takeScreenshot(ScenarioUrl+"/RoomTypeLookupFaliure.jpg", driver);
				e3.printStackTrace();
				e3.printStackTrace();
			}
			
			Thread.sleep(1000);
			driver.switchTo().defaultContent();

			try {
				driver.findElement(By.id("regionname")).clear();
				driver.findElement(By.id("regionname")).sendKeys(
						hotelsupplementary.getRegion());
				driver.findElement(By.id("regionname_lkup")).click();
				driver.switchTo().frame("lookup");
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				Thread.sleep(1000);
			} catch (Exception e3) {
				logger.fatal("Supplementary Setup ---> region  "+hotelsupplementary.getRegion()+" not available in the lookup");
				takeScreenshot(ScenarioUrl+"/HotelNameLookupFaliure.jpg", driver);
				e3.printStackTrace();
				return driver;
			}
			driver.switchTo().defaultContent();

			try {
				driver.findElement(By.id("touroperatorname")).clear();
				driver.findElement(By.id("touroperatorname")).sendKeys(
						hotelsupplementary.getTour_Operator());
				driver.findElement(By.id("touroperatorname_lkup")).click();
				driver.switchTo().frame("lookup");
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			} catch (Exception e3) {
				logger.fatal("Supplementary Setup ---> Tour Operator Look Up  "+hotelsupplementary.getTour_Operator()+" not available in the lookup");
				takeScreenshot(ScenarioUrl+"/TourOperatorLookupFaliure.jpg", driver);
				e3.printStackTrace();
				return driver;
			}
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
           //*** This Part added to prevent missing  0 in date ex: 1-May-2001 to 01-May-2001 in case there is a format change in excel
			String BookingFDate  = hotelsupplementary.getDate_From();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); 
			try {
				BookingFDate         = sdf.format(sdf.parse(BookingFDate));
			} catch (ParseException e1) {
				logger.fatal("Error in parsing booking date -> " +BookingFDate);
				e1.printStackTrace();
			}
			
			//*******************************************************************
			String[] partsBF = BookingFDate.split("-");
			String BookFDay = partsBF[0];
			String BookFMonth = partsBF[1];
			String BookFYear = partsBF[2];

			new org.openqa.selenium.support.ui.Select(driver.findElement(By
					.id("fromDate_Month_ID"))).selectByVisibleText(BookFMonth);
			new org.openqa.selenium.support.ui.Select(driver.findElement(By
					.id("fromDate_Day_ID"))).selectByVisibleText(BookFDay);
			new org.openqa.selenium.support.ui.Select(driver.findElement(By
					.id("fromDate_Year_ID"))).selectByVisibleText(BookFYear);

			String BookingTDate = hotelsupplementary.getDate_To();
			String[] partsBT = BookingTDate.split("-");
			String BookTDay = partsBT[0];
			String BookTMonth = partsBT[1];
			String BookTYear = partsBT[2];

			new org.openqa.selenium.support.ui.Select(driver.findElement(By
					.id("toDate_Month_ID"))).selectByVisibleText(BookTMonth);
			new org.openqa.selenium.support.ui.Select(driver.findElement(By
					.id("toDate_Day_ID"))).selectByVisibleText(BookTDay);
            new org.openqa.selenium.support.ui.Select(driver.findElement(By
					.id("toDate_Year_ID"))).selectByVisibleText(BookTYear);

			///////

			String suppApp = hotelsupplementary.getSupplementary_Applicable();
			ArrayList<String> valuelist = new ArrayList<String>(Arrays.asList(suppApp.split("#")));
			
			for (Iterator iterator = valuelist.iterator(); iterator.hasNext();) {
				String string = ((String) iterator.next()).replace(" ", "").trim();
				string = !string.contains("ShoppingCart")?string.replace("Reservations", "Reservation"):string;  
				driver.findElement(By.id("supplementaryApplicableFor".concat(string))).click();
			}
		/*	
			List<String> valueList = new ArrayList<String>();
			
			for (String value : partsEA) {
				valueList.add(value);
			}
			
			int[] list = new int[4];
			for (int i = 1; i < list.length; i++) {

				WebElement ele = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[4]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td[3]/table[2]/tbody/tr/td[2]/table/tbody/tr/td[3]" +
						"/table/tbody/tr[" +i+ "]/td[2]"));

				String suppApplyBy = ele.findElement(By.tagName("label")).getText();
				
				for(int a=0; a<valueList.size(); a++){
					
					if(valueList.get(a).equals(suppApplyBy)){
						
						String idact = ele.findElement(By.tagName("label")).getAttribute("id");
						driver.findElement(By.id(idact)).click();
						
						break;
					}
				}
			}*/

			
			////////

			if (hotelsupplementary.getMendatory().equals("No")) {

				driver.findElement(By.xpath(".//*[@id='isMandatory_N']")).click();
			}

			if (hotelsupplementary.getActive().equals("No")) {

				driver.findElement(By.xpath(".//*[@id='isActive_N']")).click();
			}

			// /
			if (hotelsupplementary.getRateBased_On().equals("Per Service")) {

				driver.findElement(By.xpath(".//*[@id='rateBasedDetail_PS']"))
						.click();

				driver.findElement(By.id("netRate")).sendKeys(
						hotelsupplementary.getNetRate());

				if (hotelsupplementary.getProfit_By().equals("Percentage")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_P']"))
							.click();
				}

				if (hotelsupplementary.getProfit_By().equals("Fixed Value")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_F']"))
							.click();
				}

				driver.findElement(By.id("profitMarkup")).sendKeys(
						hotelsupplementary.getProfit_Value());

			}

			if (hotelsupplementary.getRateBased_On().equals("Per Person")) {

				driver.findElement(By.xpath(".//*[@id='rateBasedDetail_PP']"))
						.click();

				driver.findElement(By.id("netRate")).sendKeys(
						hotelsupplementary.getNetRate());
				driver.findElement(By.id("childNetRate")).sendKeys(
						hotelsupplementary.getChild_NetRate());

				if (hotelsupplementary.getProfit_By().equals("Percentage")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_P']"))
							.click();
				}

				if (hotelsupplementary.getProfit_By().equals("Fixed Value")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_F']"))
							.click();
				}

				driver.findElement(By.id("profitMarkup")).sendKeys(
						hotelsupplementary.getProfit_Value());
				driver.findElement(By.id("childProfitMarkup")).sendKeys(
						hotelsupplementary.getChild_Profit_Value());

				String minchildage = hotelsupplementary.getMin_Child_Age();
				String maxchildage = hotelsupplementary.getMax_Child_Age();

				new org.openqa.selenium.support.ui.Select(driver.findElement(By
						.name("freechildagefrom")))
						.selectByVisibleText(minchildage);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By
						.name("freechildageto"))).selectByVisibleText(maxchildage);

			}

			if (hotelsupplementary.getRateBased_On().equals("Per Day")) {

				driver.findElement(By.xpath(".//*[@id='rateBasedDetail_PD']"))
						.click();

				driver.findElement(By.id("netRate")).sendKeys(
						hotelsupplementary.getNetRate());

				if (hotelsupplementary.getProfit_By().equals("Percentage")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_P']"))
							.click();
				}

				if (hotelsupplementary.getProfit_By().equals("Fixed Value")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_F']"))
							.click();
				}

				driver.findElement(By.id("profitMarkup")).sendKeys(
						hotelsupplementary.getProfit_Value());

			}

			if (hotelsupplementary.getRateBased_On().equals("Per Person Per Day")) {

				driver.findElement(By.xpath(".//*[@id='rateBasedDetail_PPPD']"))
						.click();

				driver.findElement(By.id("netRate")).sendKeys(
						hotelsupplementary.getNetRate());
				driver.findElement(By.id("childNetRate")).sendKeys(
						hotelsupplementary.getChild_NetRate());

				if (hotelsupplementary.getProfit_By().equals("Percentage")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_P']"))
							.click();
				}

				if (hotelsupplementary.getProfit_By().equals("Fixed Value")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_F']"))
							.click();
				}

				driver.findElement(By.id("profitMarkup")).sendKeys(
						hotelsupplementary.getProfit_Value());
				driver.findElement(By.id("childProfitMarkup")).sendKeys(
						hotelsupplementary.getChild_Profit_Value());

				String minchildage = hotelsupplementary.getMin_Child_Age();
				String maxchildage = hotelsupplementary.getMax_Child_Age();

				new org.openqa.selenium.support.ui.Select(driver.findElement(By
						.name("freechildagefrom")))
						.selectByVisibleText(minchildage);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By
						.name("freechildageto"))).selectByVisibleText(maxchildage);

			}

			if (hotelsupplementary.getRateBased_On().equals("Per Room Per Day")) {

				driver.findElement(By.xpath(".//*[@id='rateBasedDetail_PRPD']"))
						.click();

				driver.findElement(By.id("netRate")).sendKeys(
						hotelsupplementary.getNetRate());

				if (hotelsupplementary.getProfit_By().equals("Percentage")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_P']"))
							.click();
				}

				if (hotelsupplementary.getProfit_By().equals("Fixed Value")) {

					driver.findElement(By.xpath(".//*[@id='profitmarkupby_F']"))
							.click();
				}

				driver.findElement(By.id("profitMarkup")).sendKeys(
						hotelsupplementary.getProfit_Value());

			}

			if (hotelsupplementary.getRateBased_On().equals("Late Checkout")) {

				driver.findElement(By.id("percentageRoomRate")).sendKeys(
						hotelsupplementary.getPercentage_Room_Rate());
			}

			 driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
            
			try {
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

				if (driver.findElement(By.id("MainMsgBox_msgDisplayArea"))
						.isDisplayed()) {
					logger.info("MainMsgBox_msgDisplayArea is displayed");
					String ErrorText = driver.findElement(
							By.id("MainMsgBox_msgDisplayArea")).getText();
					logger.fatal("Supplimentary not Saved---->" + ErrorText);
					driver.findElement(
							By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
							.click();
					driver.findElement(
							By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img"))
							.click();
					driver.switchTo().defaultContent();
				} else if (driver.findElement(By.id("dialogMsgText"))
						.isDisplayed()) {
					logger.info("dialogMsgText displayed");
					String ErrorText = driver.findElement(
							By.id("dialogMsgText")).getText();
					logger.info("Supplimentary Saved ---->"+hotelsupplementary.getSupplementary_Name()+ "-> " + ErrorText);
					driver.findElement(By.id("dialogMsgText")).getText();
					driver.findElement(By.id("dialogMsgActionButtons")).click();
					driver.switchTo().defaultContent();
				} else {
					logger.info(CurrentHotel.getHotelName() + " , "
							+ "Supplimentary not Saved ---->"+hotelsupplementary.getSupplementary_Name());
					
				}
			} catch (Exception e) {
				try {

					String ErrorText = driver.findElement(
							By.id("dialogMsgText")).getText();
					logger.fatal("Supplimentary Not Saved ---->"+hotelsupplementary.getSupplementary_Name()+ "-> " + ErrorText);
					driver.findElement(By.id("dialogMsgText")).getText();
					driver.findElement(By.id("dialogMsgActionButtons")).click();

				} catch (Exception e2) {
					logger.info(CurrentHotel.getHotelName() + " , "
							+ "Supplimentary Saved ---->"+hotelsupplementary.getSupplementary_Name());
				}
			}
		}
		return driver;
	}

	public WebDriver createPromotionSetup(WebDriver driver) throws InterruptedException {

		ArrayList<HotelPromotion> PromotionList           = CurrentHotel.getHotelPromotions();
		Iterator<HotelPromotion>  entryList               = PromotionList.iterator();
	    String                    screenpath              = "Screenshots/PromotionSetup";
		File                      screenshotdir           = new File(screenpath);
		
		if(!screenshotdir.exists()){
			logger.info("Screenshot path not available - Creating directories..");
			
			try {
			 screenshotdir.mkdirs();
			 logger.info("Directory created succcessfully!!");
			} catch (Exception e) {
			 logger.fatal("Directory creation failed "+ e.toString());
			}
		}
		
		if((PromotionList == null) || (PromotionList.size() == 0) )
		{
			logger.info(CurrentHotel.getHotelName()+" Promotion List is empty----->");
			return driver;
		}
		int count = 1;
		while(entryList.hasNext())
		{
			try {
			 File     ScenarioPath   = new File(screenpath+"/Scenario"+count);
			 if(!ScenarioPath.exists()){
				logger.info("Screenshot path not available - Creating directories..");
				
				try {
				 new File("Screenshots/PromotionSetup/Scenario"+count).mkdirs();
				 logger.info("Directory created succcessfully!!");
				 } catch (Exception e) {
				 logger.fatal("Directory creation failed "+ e.toString());
				}
			}
			//    <----------------  Starting promotion setup ------------------------->  //
			 
           HotelPromotion  hotelPromotion = entryList.next();
           logger.info("Getting promotion setup path "+PropertySet.get("Hotel.BaseUrl")
					+ "/hotels/inventoryandrates/SpecialPromotionPage.do?module=contract");
			
           driver.get(PropertySet.get("Hotel.BaseUrl")
					+ "/hotels/inventoryandrates/SpecialPromotionPage.do?module=contract");
			Thread.sleep(500);
			
			try {
				new WebDriverWait(driver, 100).until(ExpectedConditions.presenceOfElementLocated(By.id("suppliername")));
				logger.info("Promotion crieteria selection page loaded successfully");
				takeScreenshot(screenpath+"/Scenario"+count+"/"+"CrieteriaSelectionPage.jpg", driver);
				
				try {
					driver.findElement(By.id("suppliername")).sendKeys(hotelPromotion.getSupp_Name());
					driver.findElement(By.id("suppliername_lkup")).click();		
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				} catch (Exception e) {
					logger.fatal("Promotion Setup - Supplier name not available in the look up");
				}
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				
				new WebDriverWait(driver, 100).until(ExpectedConditions.presenceOfElementLocated(By.id("hotelName")));
				
			
				try {
					driver.findElement(By.id("hotelName")).sendKeys(hotelPromotion.getHotel_Name());
					driver.findElement(By.id("hotelName_lkup")).click();		
					driver.switchTo().frame("lookup");
					
					if(hotelPromotion.getHotel_Name().trim().equalsIgnoreCase("ALL"))
					 driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					else
					 driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
				} catch (Exception e) {
					logger.fatal("Promotion Setup - Hotel Name not available in the look up");
				}
				
				
				
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				
				try {
					driver.findElement(By.id("roomType")).clear();
					driver.findElement(By.id("roomType")).sendKeys(hotelPromotion.getRoom_Type());
					driver.findElement(By.id("roomType_lkup")).click();		
					driver.switchTo().frame("lookup");
					
					if(hotelPromotion.getRoom_Type().trim().equalsIgnoreCase("ALL"))
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					else
					driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
					
				} catch (Exception e) {
					logger.fatal("Promotion Setup - Room Type not available in the look up");
				}
				
		
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				
				try {
					driver.findElement(By.id("bedTypeName")).clear();
					driver.findElement(By.id("bedTypeName")).sendKeys(hotelPromotion.getBed_Type());
					driver.findElement(By.id("bedTypeName_lkup")).click();		
					driver.switchTo().frame("lookup");
					
					if(hotelPromotion.getBed_Type().trim().equalsIgnoreCase("ALL"))
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					else
					driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
					
				} catch (Exception e) {
					logger.fatal("Promotion Setup - Bed Type not available in the look up");
				}
				
				
				
				
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				
				try {
					driver.findElement(By.id("ratePlanName")).clear();
					driver.findElement(By.id("ratePlanName")).sendKeys(hotelPromotion.getRate_Plan());
					driver.findElement(By.id("ratePlanName_lkup")).click();		
					driver.switchTo().frame("lookup");
					
					if(hotelPromotion.getRate_Plan().trim().equalsIgnoreCase("ALL"))
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					else
					driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
					
				} catch (Exception e) {
					logger.fatal("Promotion Setup - Rate Plan not available in the look up");
				}
				
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				
				driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img")).click();
				Thread.sleep(1000);
				
				try {
					new WebDriverWait(driver, 100).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='calculationlogictype_S']")));
					logger.info("Promotion Setup - Promotion SetUp Window Available");
					takeScreenshot(screenpath+"/Scenario"+count+"/"+"PromotionSetUpPage.jpg", driver);
				} catch (Exception e) {
					logger.info("Promotion Setup - Promotion SetUp Window Not Available");
					takeScreenshot(screenpath+"/Scenario"+count+"/"+"PromotionSetUpPageNotLoaded.jpg", driver);
				}
				
				if (hotelPromotion.getCalculation_Logic().equals("Standard Adults")) {
					
					driver.findElement(By.xpath(".//*[@id='calculationlogictype_S']")).click();			
				}
				
				//1
				if (hotelPromotion.getPromotion_BasedOn() == PromotionBasedOnType.MINIMUMNIGHTSBOOKED) {
				
					driver.findElement(By.xpath(".//*[@id='promotion_minnights']")).click();	
				    driver.findElement(By.id("noofNightsreq")).sendKeys(hotelPromotion.getNights_Book());
					
					//1.1
					if (hotelPromotion.getPromotionType() == PromotionType.FREENIGHTS) {
						
						driver.findElement(By.xpath(".//*[@id='prombasedon_F']")).click();
						driver.findElement(By.id("noofFreeNights")).sendKeys(hotelPromotion.getFN_FreeNights());
						driver.findElement(By.id("maxnoofNights")).sendKeys(hotelPromotion.getFN_MaxNumberofNights());
						driver.findElement(By.id("surcharge")).sendKeys(hotelPromotion.getFN_Surchage_Fee());
										
					}
					
					//1.2
					if (hotelPromotion.getPromotionType() == PromotionType.DISCOUNT) {
						
						driver.findElement(By.xpath(".//*[@id='prombasedon_D']")).click();
						
						if (hotelPromotion.getDiscountrateapplicabletype() == DiscountRateApplicableType.PERCENTAGE) 
						driver.findElement(By.xpath(".//*[@id='prombystatus_P']")).click();
					
						if (hotelPromotion.getDiscountrateapplicabletype()==DiscountRateApplicableType.VALUE) 
						driver.findElement(By.xpath(".//*[@id='prombystatus_V']")).click();
						
						driver.findElement(By.id("discountvalue")).sendKeys(hotelPromotion.getPromo_Value());
						
					}
					
					//1.3
					if (hotelPromotion.getPromotionType() == PromotionType.RATE) {
						
						driver.findElement(By.xpath(".//*[@id='prombasedon_R']")).click();
						
						if (hotelPromotion.getDiscountrateapplicabletype() == DiscountRateApplicableType.NETRATE) 
						driver.findElement(By.xpath(".//*[@id='prombyRatetype_N']")).click();
						
						if (hotelPromotion.getDiscountrateapplicabletype() == DiscountRateApplicableType.SELLRATE) 
						driver.findElement(By.xpath(".//*[@id='prombyRatetype_S']")).click();
						
						
						driver.findElement(By.id("rateValue")).sendKeys(hotelPromotion.getPromo_Value());
						
					}
					
					//1.4
					if (hotelPromotion.getPromotionType() == PromotionType.FREENIGHTSANDARATE) {
						
						driver.findElement(By.xpath(".//*[@id='prombasedon_FR']")).click();
						
						driver.findElement(By.id("noofFreeNights")).sendKeys(hotelPromotion.getFN_FreeNights());
						driver.findElement(By.id("maxnoofNights")).sendKeys(hotelPromotion.getFN_MaxNumberofNights());
						driver.findElement(By.id("surcharge")).sendKeys(hotelPromotion.getFN_Surchage_Fee());
						
						if (hotelPromotion.getSpecialRateApplicableType() == PromotionSpecialRateApplicableType.PROMOPERIOD)
						driver.findElement(By.xpath(".//*[@id='promoSpecialRate_P']")).click();
												
						if (hotelPromotion.getSpecialRateApplicableType() == PromotionSpecialRateApplicableType.FOREXTRANIGHT) 
						driver.findElement(By.xpath(".//*[@id='promoSpecialRate_E']")).click();
						
						if (hotelPromotion.getSpecialRateApplicableType() == PromotionSpecialRateApplicableType.BOTH)
						driver.findElement(By.xpath(".//*[@id='promoSpecialRate_BO']")).click();
						
						if (hotelPromotion.getDiscountrateapplicabletype() == DiscountRateApplicableType.NETRATE) 
						driver.findElement(By.xpath(".//*[@id='prombyRatetype_N']")).click();
						
						if (hotelPromotion.getDiscountrateapplicabletype() == DiscountRateApplicableType.SELLRATE)
						driver.findElement(By.xpath(".//*[@id='prombyRatetype_S']")).click();
					
						driver.findElement(By.id("rateValue")).sendKeys(hotelPromotion.getPromo_Value());
										
					}			
				}
			
				//2
				if (hotelPromotion.getPromotion_BasedOn() == PromotionBasedOnType.PROMOPERIOD) {
					
					driver.findElement(By.xpath(".//*[@id='promotion_promotionperiod']")).click();
					
					if (hotelPromotion.getPromotionType() == PromotionType.DISCOUNT) {
						
						driver.findElement(By.xpath(".//*[@id='prombytype_D']")).click();
						
						if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.PERCENTAGE) 
						 driver.findElement(By.xpath(".//*[@id='prombystatus_P']")).click();
                       if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.VALUE) 
						 driver.findElement(By.xpath(".//*[@id='prombystatus_V']")).click();
					   
                       driver.findElement(By.id("promotiondiscount")).sendKeys(hotelPromotion.getPromo_Value());
					}
					
					if (hotelPromotion.getPromotionType() == PromotionType.RATE) {
						
						driver.findElement(By.xpath(".//*[@id='prombytype_R']")).click();
						
						if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.NETRATE)
						 driver.findElement(By.xpath(".//*[@id='prombyRatetype_N']")).click();
                       if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.SELLRATE) 
						 driver.findElement(By.xpath(".//*[@id='prombyRatetype_S']")).click();
					
						driver.findElement(By.id("rateValue")).sendKeys(hotelPromotion.getPromo_Value());
						
					}
								
				}
				
				
				//3
				if (hotelPromotion.getPromotion_BasedOn()== PromotionBasedOnType.EARLYBIRD) {
					
					driver.findElement(By.xpath(".//*[@id='promotion_earlybird']")).click();
					driver.findElement(By.id("noofNights")).sendKeys(hotelPromotion.getPrior_Arrival());
					
					if (hotelPromotion.getPromotionType() == PromotionType.DISCOUNT) {
						
						driver.findElement(By.xpath(".//*[@id='prombytype_D']")).click();
						if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.PERCENTAGE) 
					     driver.findElement(By.xpath(".//*[@id='prombystatus_P']")).click();
	                    if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.VALUE) 
						 driver.findElement(By.xpath(".//*[@id='prombystatus_V']")).click();
						   
						driver.findElement(By.id("earlydiscount")).sendKeys(hotelPromotion.getPromo_Value());
					}
					
					if (hotelPromotion.getPromotionType() == PromotionType.RATE) {
						
						driver.findElement(By.xpath(".//*[@id='prombytype_R']")).click();
						if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.NETRATE)
					     driver.findElement(By.xpath(".//*[@id='prombyRatetype_N']")).click();
	                    if (hotelPromotion.getDiscountrateapplicabletype()== DiscountRateApplicableType.SELLRATE) 
						 driver.findElement(By.xpath(".//*[@id='prombyRatetype_S']")).click();
						
						driver.findElement(By.id("rateValue")).sendKeys(hotelPromotion.getPromo_Value());
						
					}
								
				}
				
				driver.findElement(By.id("note")).sendKeys(hotelPromotion.getNote());
				
				///////////////////////
				
				//Booking date
				if (hotelPromotion.getApplicablePeriod() == PromotionApplicablePeriodType.BOOKINGDATE) {
					
					driver.findElement(By.xpath(".//*[@id='appliedTo_BK']")).click();
					
					String BookingFDate = hotelPromotion.getBooking_Date_From();
					String[] partsBF = BookingFDate.split("-");
					String BookFDay = partsBF[0]; 
					String BookFMonth = partsBF[1]; 
					String BookFYear = partsBF[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingFrom_Month_ID"))).selectByVisibleText(BookFMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingFrom_Day_ID"))).selectByVisibleText(BookFDay);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingFrom_Year_ID"))).selectByVisibleText(BookFYear);
					
					String BookingTDate = hotelPromotion.getBooking_Date_To();
					String[] partsBT = BookingTDate.split("-");
					String BookTDay = partsBT[0]; 
					String BookTMonth = partsBT[1]; 
					String BookTYear = partsBT[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingTo_Month_ID"))).selectByVisibleText(BookTMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingTo_Day_ID"))).selectByVisibleText(BookTDay);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingTo_Year_ID"))).selectByVisibleText(BookTYear);
								
				}
				
				
				//Stay Date Period
				if (hotelPromotion.getApplicablePeriod()== PromotionApplicablePeriodType.STAYDATE) {
					
					driver.findElement(By.xpath(".//*[@id='appliedTo_AR']")).click();
					
					String StayFDate = hotelPromotion.getBooking_Date_From();
					String[] partsSF = StayFDate.split("-");
					String StayFDay = partsSF[0]; 
					String StayFMonth = partsSF[1]; 
					String StayFYear = partsSF[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalFrom_Month_ID"))).selectByVisibleText(StayFMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalFrom_Year_ID"))).selectByVisibleText(StayFYear);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalFrom_Day_ID"))).selectByVisibleText(StayFDay);
					
								
					String StayTDate = hotelPromotion.getBooking_Date_To();
					String[] partsST = StayTDate.split("-");
					String StayTDay = partsST[0]; 
					String StayTMonth = partsST[1]; 
					String StayTYear = partsST[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalTo_Month_ID"))).selectByVisibleText(StayTMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalTo_Year_ID"))).selectByVisibleText(StayTYear);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalTo_Day_ID"))).selectByVisibleText(StayTDay);
				
				}
				
				
				//Both
				if (hotelPromotion.getApplicablePeriod() == PromotionApplicablePeriodType.BOTH) {
					
					driver.findElement(By.xpath(".//*[@id='appliedTo_BO']")).click();
					
					String BothBookingFDate = hotelPromotion.getBooking_Date_From();
					String[] partsBBF = BothBookingFDate.split("-");
					String BoBookFDay = partsBBF[0]; 
					String BoBookFMonth = partsBBF[1]; 
					String BoBookFYear = partsBBF[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingFrom_Month_ID"))).selectByVisibleText(BoBookFMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingFrom_Day_ID"))).selectByVisibleText(BoBookFDay);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingFrom_Year_ID"))).selectByVisibleText(BoBookFYear);
					
					String BothBookingTDate = hotelPromotion.getBooking_Date_To();
					String[] partsBBT = BothBookingTDate.split("-");
					String BoBookTDay = partsBBT[0]; 
					String BoBookTMonth = partsBBT[1]; 
					String BoBookTYear = partsBBT[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingTo_Month_ID"))).selectByVisibleText(BoBookTMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingTo_Day_ID"))).selectByVisibleText(BoBookTDay);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("bookingTo_Year_ID"))).selectByVisibleText(BoBookTYear);
					
					////////
					
					String BoothStayFDate = hotelPromotion.getStay_Date_From();
					String[] partsBSF = BoothStayFDate.split("-");
					String BoStayFDay = partsBSF[0]; 
					String BoStayFMonth = partsBSF[1]; 
					String BoStayFYear = partsBSF[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalFrom_Month_ID"))).selectByVisibleText(BoStayFMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalFrom_Day_ID"))).selectByVisibleText(BoStayFDay);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalFrom_Year_ID"))).selectByVisibleText(BoStayFYear);
								
					String BothStayTDate = hotelPromotion.getStay_Date_To();
					String[] partsBST = BothStayTDate.split("-");
					String BoStayTDay = partsBST[0]; 
					String BoStayTMonth = partsBST[1]; 
					String BoStayTYear = partsBST[2]; 
					
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalTo_Month_ID"))).selectByVisibleText(BoStayTMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalTo_Day_ID"))).selectByVisibleText(BoStayTDay);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("arrivalTo_Year_ID"))).selectByVisibleText(BoStayTYear);
								
				}
				
				if (hotelPromotion.getBooking_Channel().equals("All")) {
					
					driver.findElement(By.xpath(".//*[@id='channel_ALL']")).click();
				}
				
				if (hotelPromotion.getBooking_Channel().equals("Website")) {
						
					driver.findElement(By.xpath(".//*[@id='channel_WEB']")).click();
				}
						
				if (hotelPromotion.getBooking_Channel().equals("Call Center")) {
					
					driver.findElement(By.xpath(".//*[@id='channel_CC']")).click();
				}
				
				///
				
				if (hotelPromotion.getPartnerType() == PartnerType.ALL) {
					
					driver.findElement(By.xpath(".//*[@id='customerType_ALL']")).click();
				}
				
				if (hotelPromotion.getPartnerType() == PartnerType.DIRECT) {
							
					driver.findElement(By.xpath(".//*[@id='customerType_DC']")).click();		
				}
						
				if (hotelPromotion.getPartnerType() == PartnerType.AFFLIATE) {
					
					driver.findElement(By.xpath(".//*[@id='customerType_AFF']")).click();
					
					driver.findElement(By.id("affiliateName")).sendKeys(hotelPromotion.getAgentName());
					driver.findElement(By.id("affiliateName_lkup")).click();		
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					
				}
				
				if (hotelPromotion.getPartnerType() == PartnerType.TRAVELAGENT) {
					
					driver.findElement(By.xpath(".//*[@id='customerType_TA']")).click();
					
					driver.findElement(By.id("travelAgentName")).sendKeys(hotelPromotion.getAgentName());
					driver.findElement(By.id("travelAgentName_lkup")).click();		
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					
				}
				
				if (hotelPromotion.getPartnerType() == PartnerType.TOUROPERATOR) {
					
					driver.findElement(By.xpath(".//*[@id='customerType_TO']")).click();
					
					driver.findElement(By.id("regionname")).clear();
					driver.findElement(By.id("regionname")).sendKeys(hotelPromotion.getAgentRegion());
					driver.findElement(By.id("regionname_lkup")).click();		
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					
					driver.findElement(By.id("name")).clear();
					driver.findElement(By.id("name")).sendKeys(hotelPromotion.getAgentName());
					driver.findElement(By.id("name_lkup")).click();		
					driver.switchTo().frame("lookup");
					driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
				}
				
				if (hotelPromotion.isIs_Active() == true) {
					
					driver.findElement(By.xpath(".//*[@id='isActive_Y']")).click();
				}
				
				if (hotelPromotion.isIs_Active() == false) {
					
					driver.findElement(By.xpath(".//*[@id='isActive_N']")).click();
				}
				
				
				////save
				driver.findElement(By.className("standalonelink")).click();
				////
		
				try {
					WebDriverWait wait = new WebDriverWait(driver, 60);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
					
                   if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
						
						String TextMessage   = driver.findElement(By.id("dialogMsgText")).getText();
						logger.info(" Promotion  Saved ---->"+hotelPromotion.getNote()+ "-> " + TextMessage);
						driver.findElement(By.id("dialogMsgActionButtons")).click();
						takeScreenshot(screenpath+"/Scenario"+count+"/"+"PromosavedSuccessfully.jpg", driver);
						driver.switchTo().defaultContent();
						driver.findElement(
								By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
								.click();
					} else {

						String ErrorText = driver.findElement(
								By.id("MainMsgBox_msgDisplayArea")).getText();
						logger.fatal(hotelPromotion.getNote()+" Promotion not saved---->" + ErrorText);
						driver.findElement(
								By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img"))
								.click();
						/*driver.findElement(
								By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img"))
								.click();*/
						takeScreenshot(screenpath+"/Scenario"+count+"/"+"PromosavingFailed.jpg", driver);
					}

				} catch (Exception e) {
					logger.fatal(hotelPromotion.getNote()+" Promotion not saved---->" + e.toString());;
					//System.out.println(e.toString());

				}
			} catch (Exception e) {
				logger.fatal("Promotion crieteria selection page not  loaded successfully "+e.toString());
				takeScreenshot(screenpath+"/Scenario"+count+"/"+"SelectionpageFailedScenario.jpg", driver);
			}
			
			} catch (Exception e) {
			logger.fatal("Promotion setup Failed "+e.getMessage());
			takeScreenshot(screenpath+"/Scenario"+count+"/"+"FailedScenario.jpg", driver);
		}
		}
		return driver;
	}

	public boolean isElementPresent(By by, WebElement element, WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			element.findElement(by);
			return true;
		} catch (Exception ex) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}

	public void acceptAlert(WebDriver driver) {

		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();

		} catch (Exception e) {

		}
	}
	
	public void takeScreenshot(String path,WebDriver driver) {
		
		try {
			logger.info("Creating screenshot on path-->"+path);
			FileUtils.copyFile(((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE),new File(path));	
			logger.info("Screenshot Saved Successfully..!!!");
		} catch (Exception e) {
			logger.fatal("Screenshot Saving Failed "+e.toString());
		}
	}

}
