package setup.runners.mainsetup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookingFee {
	
	
	
	public WebDriver HotelBookingFeePage(WebDriver driver) throws InterruptedException {
		
		driver.get(Repository.BaseUrl.replace("admin/common/LoginPage.do",
				"admin/setup/BookingFeeSetupAction.do?ActionType=submit"));
		
		System.out.println("Succussfully loaded Hotel Booking Fee");
		
		
		//Select Country
		
		driver.findElement(By.id("countryName")).sendKeys(Repository.Country);
		driver.findElement(By.id("countryName_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		//Select City
		
		driver.findElement(By.id("cityName")).sendKeys(Repository.Country);
		driver.findElement(By.id("cityName_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		//Tax & Fee Type
		
		if (Repository.TaxFee.equals("Percentage")) {

			driver.findElement(By.id("feeType_P")).click();
		} else {
			driver.findElement(By.id("feeType_V")).click();
		}
		
		//Amount
		
		driver.findElement(By.id("amount")).sendKeys(Repository.Amount);
		
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img"))
		.click();
		
		
		return driver;
	}
	}
	
	
	

